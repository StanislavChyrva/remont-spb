const gulp = require('gulp');
const { series, parallel } = require('gulp');
const autoPrefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();
const terser = require('gulp-terser');


function style() {
    return gulp.src('src/css/**/*.css')
        .pipe(concat('calculator.css'))
        .pipe(autoPrefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        // .pipe(cleanCSS({level: 2}))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
}


function script() {
    return gulp.src('src/js/**/*.js')
        .pipe(concat('calculator-all.js'))
        // .pipe(uglify({
        //     toplevel: true
        // }))
        // .pipe(terser())
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
}

function image() {
    return gulp.src('src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
        .pipe(browserSync.stream());
}

function cleanCss() {
    return gulp.src('dist/css', {read: false})
        .pipe(clean());
}

function cleanJs() {
    return gulp.src('dist/js', {read: false})
        .pipe(clean());
}

function cleanImg() {
    return gulp.src('dist/img', {read: false})
        .pipe(clean());
}


function watch() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('src/css/**/*.css', style);
    gulp.watch('src/js/**/*.js', script);
    gulp.watch('src/img/*', image);
    gulp.watch("dist/*.html").on('change', browserSync.reload);
}


exports.build = series(parallel(cleanCss, cleanJs, cleanImg), parallel(style, script, image));
exports.dev = series(exports.build, watch);

