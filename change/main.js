import {createMiniSliders, changeMiniSlider} from "./mini-slider.js";


// init mini-sliders lazy load
let miniSliderLoaded = false; //true if mini slider loaded
const miniSliderContainer = document.getElementById('mini-slider');

function lazyLoadMiniSlider() {
    let height = window.innerHeight;
    if (!miniSliderLoaded) {
        if (pageYOffset >= $(miniSliderContainer).offset().top - height * 1.4) {
            miniSliderLoaded = true;
        }
    }
}


function onLoad() {
    createMiniSliders('studio');
}

// init touch-slider
document.addEventListener("DOMContentLoaded", onLoad);


let _changeCategoryAnimationFlow = false;

function changeMenu(category) {
    console.log(category);
    const menu = document.querySelector('.slider__category-container-secondary');
    if (category === 'design') {
        menu.classList.remove('repair');
        menu.classList.add('design');
    } else if (category === 'repair') {
        menu.classList.remove('design');
        menu.classList.add('repair');
    }
}

function changeCategory({target}) {
    if (target.classList.contains('active')) {
        return 'active category'
    } else if (!_changeCategoryAnimationFlow) {
        _changeCategoryAnimationFlow = true;
        setTimeout(() => {
            _changeCategoryAnimationFlow = false;
        }, 600);


        const buttonCollection = document.querySelectorAll('.js-category-button');
        const globalButtonCollection = document.querySelectorAll('.category-container-global__span');
        const category = target.dataset.category;
        const globalIdent = target.dataset.globalIdentifier;

        buttonCollection.forEach(item => {
            item.classList.remove('active');
            if (item.dataset.category === category) {
                item.classList.add('active');
            }

            if (item.dataset.globalCategory === globalIdent) {
                item.classList.add('active');
            }
        });

        globalButtonCollection.forEach(item => {
            item.classList.remove('active');
            if (item.dataset.globalCategory === globalIdent) {
                item.classList.add('active');
            }

            if (target.classList.contains('category-container-global__span') && globalIdent === 'design') {
                document.querySelector('[data-category="modern"]').classList.add('active');
            }
        });

        target.classList.add('active');
        changeMenu(globalIdent);

        changeMiniSlider(category);
        console.log(globalIdent);
    }
}

const buttonCollection = document.querySelectorAll('.js-category-button');
buttonCollection.forEach(item => item.addEventListener('click', changeCategory));

