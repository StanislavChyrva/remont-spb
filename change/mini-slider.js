class MiniSLider {
    constructor({containerId, category, numberOfSlider, imgQuantity, projectLink, projectDescription, timing, materials, repair, imageFormat = 'jpg'}, portfolio = false) {
        this.containerId = containerId;
        this.category = category;
        this.imgQuantity = +imgQuantity;
        this.numberOfSlider = +numberOfSlider;
        this.imageFormat = imageFormat;
        this.projectDescription = projectDescription;
        this.projectLink = projectLink;
        this.timing = timing;
        this.materials = materials;
        this.repair = repair;
        this.portfolio = portfolio; //true if use mini-slider in portfolio
        this.elem = null;
    }

    //
    render() {
        this.elem = document.createElement('div');
        this.elem.className = `mini-slider__main-container`;

        //when use in portfolio
        if (this.portfolio) {
            this.elem.innerHTML = `
          <!-- Add Arrows -->
            <div class="mini-slider__swiper-button-next mini-slider__swiper-button-next-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="../img/slider-next-arrow.svg" alt=""></div></div>
            <div class="mini-slider__swiper-button-prev mini-slider__swiper-button-prev-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="../img/slider-prev-arrow.svg" alt=""></div></div>

            <div class="mini-slider__main">
                <div class="mini-slider__swiper-container mini-slider__swiper-container-${this.numberOfSlider}">
                    <!-- Additional required wrapper -->
                    <div class="mini-slider__swiper-wrapper swiper-wrapper">
                        <!-- Slides -->
                         ${this.renderImages().join(' ')}

                    </div>
                    <!-- If we need pagination -->
<!--                    <div class="mini-slider__swiper-pagination"></div>-->
                </div>
            </div>
            
           <div class="mini-slider__description">
                    <span class="mini-slider__description-title">${this.projectDescription}</span>
                    <div class="mini-slider__description-items-wrapper">
                            <div class="mini-slider__description-item">
                                <div class="mini-slider__description-item__title">Сроки выполнения</div>
                                <div class="mini-slider__description-item__content">${this.timing.split(' ')[0]}<span>${this.timing.split(' ')[1]}</span></div>
                            </div>
                            <div class="mini-slider__description-item">
                                <div class="mini-slider__description-item__title">Материалы:</div>
                                <div class="mini-slider__description-item__content">${this.materials}<span>руб./м²</span></div>
                            </div>
                            <div class="mini-slider__description-item">
                                <div class="mini-slider__description-item__title">Ремонт:</div>
                                <div class="mini-slider__description-item__content">${this.repair}<span>руб./м²</span></div>
                            </div>
                            <a class="mini-slider__description-link" href="${this.projectLink}">Подробнее</a>
                    </div>
            </div>
        `;

            //when use in main page
        } else {
            this.elem.innerHTML = `
          <!-- Add Arrows -->
            <div class="mini-slider__swiper-button-next mini-slider__swiper-button-next-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="img/slider-next-arrow.svg" alt=""></div></div>
            <div class="mini-slider__swiper-button-prev mini-slider__swiper-button-prev-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="img/slider-prev-arrow.svg" alt=""></div></div>

            <div class="mini-slider__main">
                <div class="mini-slider__swiper-container mini-slider__swiper-container-${this.numberOfSlider}">
                    <!-- Additional required wrapper -->
                    <div class="mini-slider__swiper-wrapper swiper-wrapper">
                        <!-- Slides -->
                         ${this.renderImages().join(' ')}

                    </div>
                    <!-- If we need pagination -->
<!--                    <div class="mini-slider__swiper-pagination"></div>-->
                </div>
            </div>
              <div class="mini-slider__description">
                    <span class="mini-slider__description-title">${this.projectDescription}</span>
                    <div class="mini-slider__description-items-wrapper">
                            <div class="mini-slider__description-item">
                                <div class="mini-slider__description-item__title">Сроки выполнения</div>
                                <div class="mini-slider__description-item__content">${this.timing.split(' ')[0]}<span>${this.timing.split(' ')[1]}</span></div>
                            </div>
                            <div class="mini-slider__description-item">
                                <div class="mini-slider__description-item__title">Материалы:</div>
                                <div class="mini-slider__description-item__content">${this.materials}<span>руб./м²</span></div>
                            </div>
                            <div class="mini-slider__description-item">
                                <div class="mini-slider__description-item__title">Ремонт:</div>
                                <div class="mini-slider__description-item__content">${this.repair}<span>руб./м²</span></div>
                            </div>
                            <a class="mini-slider__description-link" href="${this.projectLink}">Подробнее</a>
                    </div>
            </div>
        `;

        }

        return this.elem;
    }


    swiperInit() {
        let mySwiper = new Swiper(`.mini-slider__swiper-container-${this.numberOfSlider}`, {
            // Optional parameters
            direction: 'horizontal',
            loop: true,

            navigation: {
                nextEl: `.mini-slider__swiper-button-next-${this.numberOfSlider}`,
                prevEl: `.mini-slider__swiper-button-prev-${this.numberOfSlider}`,
            },
        });
    }


    // create array with image HTML
    renderImages() {
        const array = [];

        //when use in portfolio
        if (this.portfolio) {
            if (this.imgQuantity > 0) {
                for (let i = 1; i <= this.imgQuantity; i++) {
                    array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="../mini-slider/img/${this.category}/${this.numberOfSlider}/${i}.${this.imageFormat}"></div>`;
                }
            } else {
                array[0] = `<div class="pre-slide"><img class="slide-image" src="../mini-slider/img/coming-soon.${this.imageFormat}" alt="Coming Soon"></div>`;

            }

            //when use in main page
        } else {
            if (this.imgQuantity > 0) {
                for (let i = 1; i <= this.imgQuantity; i++) {
                    array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="mini-slider/img/${this.category}/${this.numberOfSlider}/${i}.${this.imageFormat}"></div>`;
                }
            } else {
                array[0] = `<div class="pre-slide"><img class="slide-image" src="mini-slider/img/coming-soon.${this.imageFormat}" alt="Coming Soon"></div>`;

            }
        }

        return array;
    }

    init() {
        setTimeout(() => {
            document.getElementById(`${this.containerId}`).append(this.render());
            setTimeout(() => {
                this.swiperInit();
            }, 20)
        }, 400);


    }

}

// set param and create 12 mini-sliders
function createMiniSliders(category, imageFormat = 'jpg') {
    let i = 1;
    miniSliderParam[category].forEach(element => {
        element.containerId = `mini-slider-container-${i}`;
        element.numberOfSlider = `${i}`;
        element.category = category;
        element.imageFormat = imageFormat;
        new MiniSLider(element).init();
        i++;
    });
}

//change mini-sliders when choose new category
function changeMiniSlider(category, imageFormat = 'jpg') {
    const miniSliderContainers = document.querySelectorAll('.mini-slider-container');
    let counter = 1;
    miniSliderContainers.forEach(container => {

        createPreShowContainer(category, container, counter);
        if (container.querySelector('.mini-slider__main-container')) {
            container.querySelector('.mini-slider__main-container').classList.add('mini-slider__fade-out');
        }
        counter++;


    });

    setTimeout(() => {
        console.log('create new ms');
        createMiniSliders(category, imageFormat);
    }, 50);

    setTimeout(()=>{
        console.log('del preshow');
        miniSliderContainers.forEach(container => container.innerHTML = '');
    }, 450);

}


//create background in mini-slider container
function createPreShowContainer(category, container, counter, imageFormat = 'jpg') {
    if (+miniSliderParam[category][counter - 1].imgQuantity > 0) {
        container.setAttribute('style', `background: url("mini-slider/img/${category}/${counter}/1.${imageFormat}");`)
    } else {
        container.setAttribute('style', `background: url("mini-slider/img/coming-soon.${imageFormat}");`)

    }
}


function deletePreShowContainer(container) {
    container.setAttribute('style', 'background: unset;')
}


export {MiniSLider};
export {createMiniSliders};
export {changeMiniSlider};

