<?php define('__ROOT__', dirname(dirname(__FILE__)));

$page_title ="Портфолио";
$page_description ="Портфолио описание";
$page_keywords ="Портфолио, ключевые слова";

?>
<?php require_once(__ROOT__."/f/php/html.php"); ?>
<?php require_once(__ROOT__."/f/php/header.php"); ?>

<!-- PAGE'S CONTENT -->
<div class="container flex" id="h1_container">
    <div id="h1">
        <h1>Наши работы</h1>
    </div>
    <div id="h1_right">
		<div class="slider__category-container-global">
            <div class="category-container-global__span active js-category-button" data-category="tworooms" data-global-category="repair" data-global-identifier="repair">Ремонт и отделка</div>
            <div class="category-container-global__span js-category-button" data-category="modern" data-global-category="design" data-global-identifier="design">Дизайн интерьера</div>
        </div>

    </div>
</div>

<div class="touch-slider-main-container" id="main_slider">
    <div class="main-container">
        <div class="slider__content-container">

            <div class="slider__category-container" id="js-slider__category-container">

            <span data-category="studio" data-global-identifier="repair"
                  class="slider__category-span active js-category-button">Квартиры-студии</span>
                <span data-global-identifier="repair" data-category="oneroom" class="slider__category-span js-category-button">1-комнатные</span>
                <span data-global-identifier="repair" data-category="tworooms" class="slider__category-span js-category-button">2-х комнатные</span>
                <span data-global-identifier="repair" data-category="threerooms" class="slider__category-span js-category-button">3 и более комнат</span>
                <span data-global-identifier="repair" data-category="comfort" class="slider__category-span js-category-button">Комфорт</span>
                <span data-global-identifier="repair" data-category="european" class="slider__category-span js-category-button">Евроремонт</span>
                <span data-global-identifier="design" data-category="design" data-global-category="design" class="slider__category-span js-category-button">Дизайнерский</span>
                <span data-global-identifier="repair" data-category="bathrooms" class="slider__category-span js-category-button">Ванные комнаты</span>
            </div>


        </div>
    </div>

    <div class="mini-slider__main-wrapper">
        <div class="slider__category-container-secondary repair">

            <div class="slider__category-container-secondary__wrapper" id="js-category-container-secondary-1">
                <span class="category-container-secondary__label">Тип ремонта</span>
                <span class="category-container-secondary__span js-category-button" data-category="cosmetic" data-global-identifier="repair">Косметический</span>
                <span class="category-container-secondary__span js-category-button" data-category="comfort" data-global-identifier="repair">Комфорт</span>
                <span class="category-container-secondary__span js-category-button" data-category="european" data-global-identifier="repair">Евростандарт</span>
                <span class="category-container-secondary__span js-category-button" data-category="design" data-global-identifier="repair">Дизайнерский</span>
            </div>

            <div class="slider__category-container-secondary__wrapper" id="js-category-container-secondary-2">
                <span class="category-container-secondary__label">Тип жилья</span>
                <span class="category-container-secondary__span active js-category-button" data-category="studio" data-global-identifier="repair">Квартиры-студии</span>
                <span class="category-container-secondary__span js-category-button" data-category="oneroom" data-global-identifier="repair">1-комнатные</span>
                <span class="category-container-secondary__span js-category-button" data-category="tworooms" data-global-identifier="repair">2-х комнатные</span>
                <span class="category-container-secondary__span js-category-button" data-category="threerooms" data-global-identifier="repair">3 и более комнат</span>
                <span class="category-container-secondary__span js-category-button" data-category="cottage" data-global-identifier="repair">Дома и коттеджи</span>
            </div>

            <div class="slider__category-container-secondary__wrapper" id="js-category-container-secondary-3">
                <span class="category-container-secondary__label">Дизайн интерьера</span>
                <span class="category-container-secondary__span js-category-button" data-category="modern" data-global-identifier="design">Современный</span>
                <span class="category-container-secondary__span js-category-button" data-category="classic" data-global-identifier="design">Классический</span>
                <span class="category-container-secondary__span js-category-button" data-category="scandinavian" data-global-identifier="design">Скандинавский</span>
                <span class="category-container-secondary__span js-category-button" data-category="artdeco" data-global-identifier="design">Арт-Деко</span>
                <span class="category-container-secondary__span js-category-button" data-category="provans" data-global-identifier="design">Прованс</span>
                <span class="category-container-secondary__span js-category-button" data-category="loft" data-global-identifier="design">Лофт</span>
            </div>

            <div class="slider__category-container-secondary__wrapper" id="js-category-container-secondary-4">
                <span class="category-container-secondary__label">Тип помещения</span>
                <span class="category-container-secondary__span js-category-button" data-category="rooms" data-global-identifier="repair">Комнаты</span>
                <span class="category-container-secondary__span js-category-button" data-category="kitchens" data-global-identifier="repair">Кухни</span>
                <span class="category-container-secondary__span js-category-button" data-category="bathrooms" data-global-identifier="repair">Ванные</span>
                <span class="category-container-secondary__span js-category-button" data-category="childrens" data-global-identifier="repair">Детские</span>
                <span class="category-container-secondary__span js-category-button" data-category="bedroom" data-global-identifier="repair">Спальни</span>
            </div>

        </div>

        <div class="mini-slider" id="mini-slider">
            <div class="mini-slider-container" id="mini-slider-container-1"></div>
            <div class="mini-slider-container" id="mini-slider-container-2"></div>
            <div class="mini-slider-container " id="mini-slider-container-3"></div>
            <div class="mini-slider-container" id="mini-slider-container-4"></div>
            <div class="mini-slider-container" id="mini-slider-container-5"></div>
            <div class="mini-slider-container" id="mini-slider-container-6"></div>
            <div class="mini-slider-container" id="mini-slider-container-7"></div>
            <div class="mini-slider-container" id="mini-slider-container-8"></div>
            <div class="mini-slider-container" id="mini-slider-container-9"></div>
            <div class="mini-slider-container" id="mini-slider-container-10"></div>
        </div>
    </div>
</div>
<!-- PAGE'S CONTENT -->

<?php require_once(__ROOT__."/f/php/footer.php"); ?>