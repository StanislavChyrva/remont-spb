<div id="feedback">
    <div class="container">
        <div id="feedback_container">
            <h2>Предоставим <span>бесплатно</span> детальный расчёт<br> стоимости всех работ и материалов</h2>
            <div id="feedback_content" class="flex">
                <div id="feedback_left">
                    <p>Наш специалист приедет,<br> проконсультирует, снимет замеры,<br> предложит варианты отделки</p>
                    <p>На следущий день Вы получаете<br> расчет стоимости работ и<br> материалов БЕСПЛАТНО</p>
                    <p>На следущий день Вы получаете<br> расчет стоимости работ и<br> материалов БЕСПЛАТНО</p>
                    <p>При заказе ремонта под ключ<br> подарим скидку или<br> разработку дизайн-проекта!</p>
                </div>
                <div id="feedback_right">
                    <form action="post" class="standard_form">
                        <label>
                            <input type="text" name="name" placeholder="Ваше имя" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ваше имя'" autocomplete="off">
                        </label>
                        <label>
                            <input type="tel" name="tel" placeholder="Контактный телефон" class="input__tel" autocomplete="off" maxlength="18">
                        </label>
                        <div class="feedback_right_btn">
                            <button class="btn"><span>ПОЛУЧИТЬ СМЕТУ</span><div class="btn_bg"></div><div class="btn_bg_hover"></div></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="feedback_bg"></div>
    </div>
</div>
<div class="container flex" id="remont_deshevle">
    <div id="remont_deshevle__top">
        <h2>Ремонт с нами будет дешевле</h2>
        <p class="h2_text">Скидки на материалы в магазинах-партнёрах до 25%</p>
        <img src="/f/remont_deshevle__left.png" id="remont_deshevle__left" width="121" height="91">
        <img src="/f/remont_deshevle__right.png" id="remont_deshevle__right" width="117" height="86">
    </div>
</div>
<div class="container flex" id="remont_deshevle__slider">
    <div class="swiper-container" id="remont_deshevle__swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                 <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/1.jpg">
                <p class="remont_deshevle__slider_text">Гипермаркет товаров строительства и ремонта, дома и интерьера</p>
                </div>

                <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/9.jpg">
                <p class="remont_deshevle__slider_text">Керамическая плитка, мрамор и керамогранит</p>
                </div>


            </div>


            <div class="swiper-slide">
             <div class="remont_deshevle__slide-wrapper">
              <img src="/f/remont_deshevle/1.png">
              <p class="remont_deshevle__slider_text">Гипермаркет товаров строительства и ремонта, дома и интерьера</p>
             </div>

              <div class="remont_deshevle__slide-wrapper">
               <img src="/f/partners/10.jpg">
               <p class="remont_deshevle__slider_text">Краски для внутренней и внешней отделки</p>
              </div>


            </div>


            <div class="swiper-slide">
              <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/2.jpg">
                <p class="remont_deshevle__slider_text">Системы отопления, водоснабжения и канализации</p>
               </div>

                <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/11.jpg">
                <p class="remont_deshevle__slider_text">Краски для внутренней и внешней отделки</p>
                 </div>

            </div>


            <div class="swiper-slide">
              <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/3.jpg">
                <p class="remont_deshevle__slider_text">Керамическая плитка, мрамор и керамогранит</p>
              </div>

               <div class="remont_deshevle__slide-wrapper">
                 <img src="/f/partners/12.jpg">
                <p class="remont_deshevle__slider_text">Гипермаркет товаров строительства и ремонта, дома и интерьера</p>
               </div>

            </div>


            <div class="swiper-slide">
              <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/4.jpg">
                <p class="remont_deshevle__slider_text">Краски для внутренней и внешней отделки</p>
              </div>

              <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/13.jpg">
                <p class="remont_deshevle__slider_text">Гипермаркет товаров строительства и ремонта, дома и интерьера</p>
              </div>

            </div>


            <div class="swiper-slide">
              <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/5.jpg">
                <p class="remont_deshevle__slider_text">Краски для внутренней и внешней отделки</p>
               </div>

               <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/14.jpg">
                <p class="remont_deshevle__slider_text">Системы отопления, водоснабжения и канализации</p>
               </div>

            </div>


            <div class="swiper-slide">
             <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/6.jpg">
                <p class="remont_deshevle__slider_text">Гипермаркет товаров строительства и ремонта, дома и интерьера</p>
             </div>

             <div class="remont_deshevle__slide-wrapper">
                 <img src="/f/partners/15.jpg">
                 <p class="remont_deshevle__slider_text">Керамическая плитка, мрамор и керамогранит</p>
              </div>

            </div>


            <div class="swiper-slide">
            <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/7.jpg">
                <p class="remont_deshevle__slider_text">Гипермаркет товаров строительства и ремонта, дома и интерьера</p>
            </div>

             <div class="remont_deshevle__slide-wrapper">
                 <img src="/f/partners/16.jpg">
                 <p class="remont_deshevle__slider_text">Краски для внутренней и внешней отделки</p>
             </div>

            </div>


            <div class="swiper-slide">
              <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/8.jpg">
                <p class="remont_deshevle__slider_text">Системы отопления, водоснабжения и канализации</p>
               </div>

               <div class="remont_deshevle__slide-wrapper">
                <img src="/f/partners/17.jpg">
                <p class="remont_deshevle__slider_text">Краски для внутренней и внешней отделки</p>
               </div>
            </div>


            <div class="swiper-slide">
             <div class="remont_deshevle__slide-wrapper">
             <img src="/f/partners/18.jpg">
             <p class="remont_deshevle__slider_text">Гипермаркет товаров строительства и ремонта, дома и интерьера</p>
             </div>
            </div>

        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
<div class="container flex" id="remont_types">
    <a id="remont_types__cosmetic" href="#" data-aos="flip-left">
        <span class="remont_types__title">Косметический</span>
        <span class="remont_types__text">Лёгкая отделка квартиры<br> с минимальными затратами</span>
        <span class="remont_types__price">от <i>1000</i> руб./м<sup>2</sup></span>
        <span class="remont_types__btn">СМОТРЕТЬ</span>
    </a>
    <a id="remont_types__classic" href="#" data-aos="flip-left">
        <span class="remont_types__title">Комфорт</span>
        <span class="remont_types__text">Классический ремонт<br> и стандартный объём работ</span>
        <span class="remont_types__price">от <i>3000</i> руб./м<sup>2</sup></span>
        <span class="remont_types__btn">СМОТРЕТЬ</span>
    </a>
    <a id="remont_types__euro" href="#" data-aos="flip-left">
        <span class="remont_types__title">Евроремонт</span>
        <span class="remont_types__text">Ремонт &laquo;Бизнес&raquo; класса<br> по европейским стандартам</span>
        <span class="remont_types__price">от <i>5500</i> руб./м<sup>2</sup></span>
        <span class="remont_types__btn">СМОТРЕТЬ</span>
    </a>
    <a id="remont_types__design" href="#" data-aos="flip-left">
        <span class="remont_types__title">Дизайн + ремонт</span>
        <span class="remont_types__text">Разработка проекта<br> и ремонт под ключ</span>
        <span class="remont_types__price">от <i>8000</i> руб./м<sup>2</sup></span>
        <span class="remont_types__btn">СМОТРЕТЬ</span>
    </a>
</div>
<div class="container flex" id="footer_main__nav_container">
    <div id="footer_main__nav" class="flex">
        <div class="footer_main__nav_container">
            <div class="footer_main__nav">
                <div>Ремонт и отделка</div>
                <nav>
                    <a href="#">Ремонт квартир</a>
                    <a href="#">Сколько стоит ремонт</a>
                    <a href="#">Заказ и доставка материалов</a>
                    <a href="#">Расценки на отделочные работы</a>
                    <a href="#">Перепланировки и согласование</a>
                </nav>
            </div>
            <div class="footer_main__nav">
                <div>Приёмка новостроек</div>
                <nav>
                    <a href="#">Приёмка квартиры от застройщика</a>
                </nav>
            </div>
        </div>
        <div class="footer_main__nav_container" id="footer_main__nav_container_right">
            <div class="footer_main__nav">
                <div>Дизайн интерьера</div>
                <nav>
                    <a href="#">Дизайн квартир</a>
                    <a href="#">Образец дизайн-проекта</a>
                    <a href="#">Дизайн-проект в подарок</a>
                    <a href="#">Подбор мебели и материалов</a>
                    <a href="#">Примеры и цены готовых интерьеров</a>
                </nav>
            </div>
            <div class="footer_main__nav">
                <div>Жителям других регионов</div>
                <nav>
                    <a href="#">Дистанционный ремонт</a>
                </nav>
            </div>
        </div>
    </div>
    <div id="footer_main__text">ВЫЕЗД СПЕЦИАЛИСТА<br> КОНСУЛЬТАЦИЯ<br> и<br> РАСЧЁТ СТОИМОСТИ<br> <span>БЕСПЛАТНО</span></div>
</div>
<div id="footer_nav">
    <div class="container flex">
        <nav>
            <a href="#">Кухни</a>
            <a href="#">Комнаты</a>
            <a href="#">Ванные и санузлы</a>
        </nav>
        <nav>
            <a href="#">Калькулятор</a>
            <a href="#">Договор на дизайн</a>
            <a href="#">Договор на ремонт</a>
        </nav>
        <nav>
            <a href="#">Наши специалисты</a>
            <a href="#">Лицензии и сертификаты</a>
            <a href="#">Отзывы и рекомендации</a>
        </nav>
        <nav>
            <a href="#">О компании</a>
            <a href="#">Вакансии</a>
            <a href="#">Контакты</a>
        </nav>
        <div id="footer_nav__btn">
            ЗАКАЗАТЬ РЕМОНТ
        </div>
    </div>
</div>
<div id="footer_container">
    <div class="container flex" id="footer">
        <div id="footer_logo">
            <a href="/" id="logo_bottom">
                <img src="/f/logo-top.svg" width="80.51" height="79.22">
            </a>
            <div id="footer_logo__text">
                <p id="footer_logo__text_top">НУЖЕН КАЧЕСТВЕННЫЙ РЕМОНТ?</p>
                <p id="footer_logo__text_bottom">Позвоните прямо сейчас!</p>
            </div>
        </div>
        <div id="footer_contacts">
            <div id="footer_contacts__tel">+7 <span>(812)</span> 425 25 00</div>
            <div id="footer_contacts__text"><span>Бесплатный замер и расчет сметы</span></div>
        </div>
    </div>
</div>
<div class="modal">
    <div class="modal-overlay modal-toggle"></div>
    <div class="modal-wrapper modal-transition">
        <div class="modal-toggle modal-close"></div>
        <div class="modal-body">
            <div class="modal-content">
                <div id="modal_logo">
                    <!-- <img src="/img/logo2.png" alt=""> -->
                </div>
                <div id="modal__content">
                    <h2>Бесплатная консультация</h2>
                    <div class="h2_text">или выезд мастера</div>
                    <form action="post">
                        <label>
                            <input type="text" name="name" placeholder="Ваше имя" autocomplete="off">
                        </label>
                        <label>
                            <input type="tel" name="tel" placeholder="Контактный телефон" class="input__tel" autocomplete="off">
                        </label>

                        <button class="btn"><span>Отправить заявку</span><div class="btn_bg"></div><div class="btn_bg_hover"></div></button>
                    </form>
                </div>
                <button class="btn modal-toggle" id="modal-hidden"><span>Закрыть</span><div class="btn_bg"></div><div class="btn_bg_hover"></div></button>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/js/swiper.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script>
let cheapSwiper = new Swiper('#remont_deshevle__swiper-container', {
 slidesPerView: 6,
// loopedSlides: 2, //because i can

     spaceBetween: 30,
     loop: true,
    navigation: {
         nextEl: '#remont_deshevle__slider .swiper-container .swiper-button-next',
       prevEl: '#remont_deshevle__slider .swiper-container .swiper-button-prev',
     },
});
</script>
<script src="/portfolio/scripts/mini-slider-param.js"></script>
<script type="module" src="/portfolio/scripts/main.js"></script>
<script src="/calculator/js/calculator-params.js"></script>
<script type="module" src="/calculator/js/calculator.js"></script>
<script src="/f/index.js"></script>
</body>
</html>