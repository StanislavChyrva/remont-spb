// import {ourWorksTabsParam, miniSliderParam} from "../mini-slider/mini-slider-param.js";

class MiniSLider {
    constructor({containerId, categoryGlobal, category, numberOfSlider, imgQuantity, projectLink, projectDescription,  projectCost, imageFormat = 'jpg'}, portfolio = false) {
        this.containerId = containerId;
        this.categoryGlobal = categoryGlobal;
        this.category = category;
        this.imgQuantity = +imgQuantity;
        this.numberOfSlider = +numberOfSlider;
        this.imageFormat = imageFormat;
        this.projectDescription = projectDescription;
        this.projectCost = projectCost;
        this.projectLink = projectLink;
        this.portfolio = portfolio; //true if use mini-slider in portfolio
        this.elem = null;
    }

    //
    render() {
        this.elem = document.createElement('div');
        this.elem.className = `swiper-container__wrapper`;
        
            this.elem.innerHTML = `
             <div class="swiper-container mini_sliders euvostandard mini-slider__swiper-container-${this.numberOfSlider}">
                        <div class="swiper-wrapper">
                              ${this.renderImages().join(' ')}
                        </div>
            
                        <div class="swiper-pagination swiper-pagination-bullets swiper-pagination-${this.numberOfSlider}" style="opacity: 0;"></div>
                        
                        <div class="mini_sliders__swiper-button-prev mini-slider__swiper-button-prev-${this.numberOfSlider}">
                            <div class="swiper-button__arrow-wrapper"><img src="/img/sliders_our_works/slider-prev-arrow.svg"
                                                                           alt=""></div>
                        </div>
                        <div class="mini_sliders__swiper-button-next mini-slider__swiper-button-next-${this.numberOfSlider}">
                            <div class="swiper-button__arrow-wrapper"><img src="/img/sliders_our_works/slider-next-arrow.svg"
                                                                           alt=""></div>
                        </div>
                    </div>
                    <div class="mini_sliders__info">
                        <span>${this.projectDescription}</span>
                        <span>Стоимость: <i>${this.projectCost}</i> руб./м²</span>
                    </div>
                    `;

        return this.elem;
    }


    swiperInit() {
        let mySwiper = new Swiper(`.mini-slider__swiper-container-${this.numberOfSlider}`, {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            pagination: {
                el: `.swiper-pagination-${this.numberOfSlider}`
            },
            navigation: {
                nextEl: `.mini-slider__swiper-button-next-${this.numberOfSlider}`,
                prevEl: `.mini-slider__swiper-button-prev-${this.numberOfSlider}`,
            },
        });
    }


    // create array with image HTML
    renderImages() {
        const array = [];

            if (this.imgQuantity > 0) {
                for (let i = 1; i <= this.imgQuantity; i++) {
                    array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="mini-slider/img/${this.categoryGlobal}/${this.category}/${this.numberOfSlider}/${i}.${this.imageFormat}"></div>`;
                }
            } else {
                array[0] = `<div class="swiper-slide"><img class="slide-image" src="mini-slider/img/coming-soon.${this.imageFormat}" alt="Coming Soon"></div>`;
            }
        return array;
    }

    init() {
        setTimeout(() => {
            document.getElementById(`${this.containerId}`).append(this.render());
            setTimeout(() => {
                this.swiperInit();
            }, 20)
        }, 400);
    }
}

const miniSliders = {
    ourWorksTabsParam: {...ourWorksTabsParam},
    miniSliderParam: {...miniSliderParam},
    categoryGlobal: 'repair',
    category: 'studio',
    param: {
        mainContainer: '#mini_sliders__container',
        tabsClass: '.slider__category-span',
        categoryClass: '.new_container__main-category-span',
        categoryActive: 'new_container__main-category-span--active',
        mainContainerHTML: `
         <div id="mini-slider-container-1"></div>
    <div id="mini-slider-container-2"></div>
    <div id="mini-slider-container-3"></div>
    <div id="mini-slider-container-4"></div>
    <div id="mini-slider-container-5"></div>
    <div id="mini-slider-container-6"></div>
    <div id="mini-slider-container-7"></div>
    <div id="mini-slider-container-8"></div>
    <div id="mini-slider-container-9"></div>`
    },

    changeTabsCategory: function ({target}) {
        const category = target.dataset.category;

        this.categoryGlobal = category;
        this.category = this.ourWorksTabsParam[category][0].tabsCategory;

        const tabsCollection = document.querySelectorAll(this.param.tabsClass);
        const categoryCollection = document.querySelectorAll(this.param.categoryClass);
        const mainContainer = document.querySelector(this.param.mainContainer);


        if (category === 'design') {
            tabsCollection[tabsCollection.length-1].classList.add('opacity-0');
        } else {
            tabsCollection[tabsCollection.length-1].classList.remove('opacity-0');
        }

        categoryCollection.forEach(elem => elem.classList.remove(this.param.categoryActive));
        target.classList.add(this.param.categoryActive);

        try {
            tabsCollection.forEach((element, number) => {
                element.innerHTML = this.ourWorksTabsParam[category][number].innerText;
                element.dataset.category = this.ourWorksTabsParam[category][number].tabsCategory;
            });
        } catch (e) {
            console.log(e);
        }

        $("#mini_sliders__container").fadeTo(200,0.1);
        setTimeout(function(){
            $("#mini_sliders__container").fadeTo(400,1);
        },300);

        setTimeout(() => {
            mainContainer.innerHTML = this.param.mainContainerHTML;
            this.createMiniSliders();
            this.zeroingTabsActive();
        }, 150)

    },

    changeTabs: function({target}) {
        this.category = target.dataset.category;
        const mainContainer = document.querySelector(this.param.mainContainer);
        const tabsCollection = document.querySelectorAll(this.param.tabsClass);

        tabsCollection.forEach(elem => elem.classList.remove('active'));
        target.classList.add('active')


            $("#mini_sliders__container").fadeTo(200,0.1);
            setTimeout(function(){
                $("#mini_sliders__container").fadeTo(400,1);
            },300);

        setTimeout(() => {
            mainContainer.innerHTML = this.param.mainContainerHTML;
            this.createMiniSliders();
        }, 150)
    },

    createMiniSliders: function(imageFormat = 'jpg') {
        this.miniSliderParam[this.categoryGlobal][this.category].forEach((element, id) => {
            element.containerId = `mini-slider-container-${id+1}`;
            element.numberOfSlider = `${id+1}`;
            element.category = this.category;
            element.categoryGlobal = this.categoryGlobal;
            element.imageFormat = imageFormat;
            new MiniSLider(element).init();
        });

            this.setPaginationWidth();

    },

    zeroingTabsActive: function(){
        const tabsCollection = document.querySelectorAll(this.param.tabsClass);
        tabsCollection.forEach(elem =>
        {
            elem.classList.remove('active')
        });
        tabsCollection[0].classList.add('active');
    },

    setPaginationWidth: function(){
        setTimeout(()=> {
            if ($('.swiper-container__wrapper').width() && $(".swiper-pagination-bullet").length) {
            const slideWidth = $('.swiper-container__wrapper').width();
                $(".swiper-pagination-bullets").each(function(){
                const bullet_width = Math.floor((slideWidth) / $(this).find(".swiper-pagination-bullet").length ) - 4;
                $(this).find(".swiper-pagination-bullet").css("width", bullet_width+"px");
            });
                $(".swiper-pagination").css("opacity", "1");

            } else {
                this.setPaginationWidth();
        }
        }, 100);

    },

    addListeners: function () {
        document.querySelectorAll(this.param.categoryClass).forEach(element => element.addEventListener('click', this.changeTabsCategory.bind(this)));
        document.querySelectorAll(this.param.tabsClass).forEach(element => element.addEventListener('click', this.changeTabs.bind(this)));
    },

    init: function(){
       this.addListeners();
       this.createMiniSliders();
    }




};

miniSliders.init();

