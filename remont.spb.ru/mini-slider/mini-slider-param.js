
const ourWorksTabsParam = {
    repair:[
        {
            innerText: 'Квартиры-студии',
            tabsCategory: 'studio'
        },{
            innerText: '1-комнатные',
            tabsCategory: 'oneroom'
        },{
            innerText: '2-х комнатные',
            tabsCategory: 'tworooms'
        },{
            innerText: '3 и более комнат',
            tabsCategory: 'threerooms'
        },{
            innerText: 'Комфорт',
            tabsCategory: 'comfort'
        },{
            innerText: 'Евростандарт',
            tabsCategory: 'european'
        },{
            innerText: 'Дизайнерский',
            tabsCategory: 'design'
        },{
            innerText: 'Ванные комнаты',
            tabsCategory: 'bathrooms'
        },
    ],
    design:[
        {
            innerText: 'Современный',
            tabsCategory: 'modern'
        },{
            innerText: 'Скандинавский',
            tabsCategory: 'scandinavian'
        },{
            innerText: 'Классический',
            tabsCategory: 'classical'
        },{
            innerText: 'Арт-Деко',
            tabsCategory: 'artdeco'
        },{
            innerText: 'Прованс',
            tabsCategory: 'provans'
        },{
            innerText: 'Минимализм',
            tabsCategory: 'minimal'
        },{
            innerText: 'Лофт',
            tabsCategory: 'loft'
        },
    ],

};

const miniSliderParam = {
    repair: {
        //категория {массив объектов}
        studio:[
            {
                //колличество изображений {number}
                imgQuantity: 14,
                //текст названия проекта {string}
                projectDescription: 'ЖК "Такой-То"',
                //площадь проекта {number}
                projectCost: 100,
            },
            {
                imgQuantity: 24,
                projectDescription: 'ЖК "Другой, но похожий"',
                projectCost: 130,
            },
            {
                imgQuantity: 18,
                projectDescription: 'ЖК "Совсем другой"',
                projectCost: 10,
            },
            {
                imgQuantity: 19,
                projectDescription: 'ЖК "Такой-То"',
                 projectCost: 100,
            },
            {
                imgQuantity: 11,
                projectDescription: 'ЖК "Другой, но похожий"',
                 projectCost: 130,
            },
            {
                imgQuantity: 9,
                projectDescription: 'ЖК "Совсем другой"',
                 projectCost: 10,
            },
            {
                imgQuantity: 13,
                projectDescription: 'ЖК "Такой-То"',
                 projectCost: 100,
            },
            {
                imgQuantity: 23,
                projectDescription: 'ЖК "Другой, но похожий"',
                 projectCost: 130,
            },
            {
                imgQuantity: 11,
                projectDescription: 'ЖК "Совсем другой"',
                 projectCost: 10,
            }
        ],
        oneroom:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        tworooms:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        threerooms:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        comfort:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        european:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        design:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },

        ],
        bathrooms:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
        ],
    },
    design: {
        modern:[
            {
                //колличество изображений {number}
                imgQuantity: 4,
                //текст названия проекта {string}
                projectDescription: 'ЖК "Такой-То"',
                //площадь проекта {number}
                 projectCost: 100,
            },
            {
                imgQuantity: 2,
                projectDescription: 'ЖК "Другой, но похожий"',
                 projectCost: 130,
            },
            {
                imgQuantity: 0,
                projectDescription: 'ЖК "Совсем другой"',
                 projectCost: 10,
            },
            {
                imgQuantity: 0,
                projectDescription: 'ЖК "Такой-То"',
                 projectCost: 100,
            },
            {
                imgQuantity: 0,
                projectDescription: 'ЖК "Другой, но похожий"',
                 projectCost: 130,
            },
            {
                imgQuantity: 0,
                projectDescription: 'ЖК "Совсем другой"',
                 projectCost: 10,
            },
            {
                imgQuantity: 0,
                projectDescription: 'ЖК "Такой-То"',
                 projectCost: 100,
            },
            {
                imgQuantity: 0,
                projectDescription: 'ЖК "Другой, но похожий"',
                 projectCost: 130,
            },
            {
                imgQuantity: 0,
                projectDescription: 'ЖК "Совсем другой"',
                 projectCost: 10,
            }
        ],
        scandinavian:[
            {
                imgQuantity: 3,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 9,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        classical:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        artdeco:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        provans:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        minimal:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            }
        ],
        loft:[
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },
            {
                imgQuantity: 0,
                projectDescription: '',
                 projectCost: 0,
            },

        ],
    }

};


