// class CalculatorItem {
//     constructor({title, price}) {
//         this.title = title;
//         this.price = price;
//
//         this.elem = null;
//         this.inputValue = '';
//
//         this.animationFlow = false;
//
//     }
//
//     calculatorItemsHTML() {
//         return `
//         <span class="calculator-item__title">${this.title}</span>
//         <span class="calculator-item__price">${this.price}</span>
//
//         <div class="calculator-item__input-wrapper">
//         <div class="calculator-item__input-minus"> <img src="img/cost-calculator-input-minus.svg" alt=""></div>
//         <input class="calculator-item__input" type="number">
//         <div class="calculator-item__input-plus"><img src="img/cost-calculator-input-plus.svg" alt=""></div>
//         </div>
//
//         <span class="calculator-item__sum">0</span>
//
//         `
//     }
//
//     incrementInput() {
//         const input = this.elem.querySelector('.calculator-item__input');
//         input.value = +input.value + 1;
//         this.calculateSum();
//     }
//
//     decrementInput() {
//         const input = this.elem.querySelector('.calculator-item__input');
//         if (+input.value - 1 < 0){
//             input.value = 0;
//         } else {
//             input.value = +input.value - 1;
//         }
//         this.calculateSum();
//     }
//
//     onChangeInput() {
//         const input = this.elem.querySelector('.calculator-item__input');
//         this.inputValue = input.value;
//         input.value = '';
//     }
//
//     onBlurInput() {
//         const input = this.elem.querySelector('.calculator-item__input');
//         if (input.value === '') {
//             input.value = this.inputValue
//         }
//         this.calculateSum();
//     }
//
//     calculateSum() {
//         const sumElement = this.elem.querySelector('.calculator-item__sum');
//         const input = this.elem.querySelector('.calculator-item__input');
//         const startValue = sumElement.innerText;
//         const newValue = +input.value * +this.price;
//
//         this.animateSum(startValue, newValue, sumElement)
//     }
//
//     animateSum(startValue, newValue, sumElement) {
//         if (!this.animationFlow) {
//
//             this.animationFlow = true;
//             setTimeout(() => {
//                 this.animationFlow = false;
//             }, 250);
//
//             let sum = +startValue;
//             const step = (+newValue - +startValue) / 10;
//
//             const doStep = () => {
//                 sum += step;
//                 sumElement.innerHTML = sum;
//                 if (sum === +newValue) {
//                     return ''
//                 } else {
//                     setTimeout(() => doStep(), 20)
//                 }
//             };
//             doStep();
//         }
//     }
//
//
//     addEventListeners() {
//         this.elem.querySelector('.calculator-item__input-minus').addEventListener('click', this.decrementInput.bind(this));
//         this.elem.querySelector('.calculator-item__input-plus').addEventListener('click', this.incrementInput.bind(this));
//         this.elem.querySelector('.calculator-item__input').addEventListener('focus', this.onChangeInput.bind(this));
//         this.elem.querySelector('.calculator-item__input').addEventListener('blur', this.onBlurInput.bind(this));
//     }
//
//
//     render() {
//         console.log('render');
//         const container = document.querySelector(`.${this.containerClass}`);
//
//         this.elem = document.createElement('div');
//         this.elem.className = `${this.containerClass}__calculator-item calculator-item`;
//         this.elem.innerHTML = this.calculatorItemsHTML();
//
//         this.addEventListeners();
//         return this.elem;
//     }
//
// };


// export default CalculatorItem;
const itemProps = {
        containerClass: 'cost-calculator',
        title: 'Покраска',
        price: '150'
    };


// new CalculatorItem(itemProps).render();