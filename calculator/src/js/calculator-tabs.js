class CalculatorTab {
    constructor({text, type, size, category}) {
        this.text = text;
        this.type = type;
        this.size = size;
        this.category = category;

        this.elem = null;
        this.changeFlow = false;
    }

    render() {
        this.elem = document.createElement('div');
        this.elem.className = this.type === 'primary'
            ? 'calculator-tab calculator-tab-primary'
            : 'calculator-tab calculator-tab-secondary';
        this.elem.className += this.size === 'large' ? " calculator-tab-large" : '';
        this.elem.setAttribute('data-category', this.category);
        this.elem.innerHTML = `<span>${this.text}</span>`;
        this.elem.addEventListener('click', () => {
            if (!this.changeFlow) {
                this.changeFlow = true;
                setTimeout(() => {this.changeFlow = false;},200);
            document.querySelectorAll('.calculator-tab').forEach(item => item.classList.remove('active'));
            this.elem.classList.add('active');
        }});
        return this.elem;
    }
}

// const tabsParam = [
//     {
//         text: 'Популярные услуги',
//         type: 'primary',
//         size: 'large',
//         category: 'popular'
//     },{
//         text: 'Стены',
//         type: 'primary',
//         size: '',
//         category: 'walls'
//     },{
//         text: 'Полы',
//         type: 'primary',
//         size: '',
//         category: 'floors'
//     },{
//         text: 'Потолки',
//         type: 'primary',
//         size: '',
//         category: 'ceilings'
//     },{
//         text: 'Черновой ремонт',
//         type: 'primary',
//         size: 'large',
//         category: 'rough'
//     },{
//         text: 'Чистовой ремонт',
//         type: 'primary',
//         size: 'large',
//         category: 'fine'
//     },{
//         text: 'Сантехнические работы',
//         type: 'secondary',
//         size: '',
//         category: 'plumbing'
//     },{
//         text: 'Электромонтажные работы',
//         type: 'secondary',
//         size: '',
//         category: 'electrical'
//     },{
//         text: 'Плиточные работы',
//         type: 'secondary',
//         size: '',
//         category: 'tiling'
//     },{
//         text: 'Гипсокартон',
//         type: 'secondary',
//         size: '',
//         category: 'drywall'
//     },{
//         text: 'Демонтаж и перепланировки',
//         type: 'secondary',
//         size: '',
//         category: 'dismantling'
//     },{
//         text: 'Дизайн интерьера',
//         type: 'secondary',
//         size: '',
//         category: 'design'
//     }
// ];

