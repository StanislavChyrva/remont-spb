// import CalculatorItem from "./calculator-item";
class CalculatorItem {
    constructor({title, price}) {
        this.title = title;
        this.price = price;

        this.elem = null;
        this.inputValue = '';

        this.animationFlow = false;
        this.changeInputFlow = false;

    }

    calculatorItemsHTML() {
        return `
        <span class="calculator-item__title">${this.title}</span>
        <span class="calculator-item__price">${this.price}</span>
        
        <div class="calculator-item__input-wrapper">
        <div class="calculator-item__input-minus"> <img src="img/cost-calculator-input-minus.svg" alt=""></div>
        <div class="calculator-item__input-container">
        <input class="calculator-item__input" type="number">
        </div>
        <div class="calculator-item__input-plus"><img src="img/cost-calculator-input-plus.svg" alt=""></div>
        </div>
        
        <span class="calculator-item__sum">0</span>

        `
    }

    incrementInput() {
        if (!this.changeInputFlow) {
            this.changeInputFlow = true;
            setTimeout(() => {
                this.changeInputFlow = false;
            }, 250);
            const input = this.elem.querySelector('.calculator-item__input');
            input.value = +input.value + 1;
            this.calculateSum();
        }
    }

    decrementInput() {
        if (!this.changeInputFlow) {
            this.changeInputFlow = true;
            setTimeout(() => {
                this.changeInputFlow = false;
            }, 250);
            const input = this.elem.querySelector('.calculator-item__input');
            if (+input.value - 1 < 0) {
                input.value = 0;
            } else {
                input.value = +input.value - 1;
            }
            this.calculateSum();
        }
    }

    onChangeInput() {
        const input = this.elem.querySelector('.calculator-item__input');
        this.inputValue = input.value;
        input.value = '';
    }

    onBlurInput() {
        const input = this.elem.querySelector('.calculator-item__input');
        if (input.value === '') {
            input.value = this.inputValue
        }
        this.calculateSum();
    }

    onKeyPressInput ({target}) {
        setTimeout(() =>  this.calculateSum(), 20);
        target.value = target.value.length >= 3 ? target.value.slice(0, 2) : target.value;

    }

    calculateSum() {
        const sumElement = this.elem.querySelector('.calculator-item__sum');
        const input = this.elem.querySelector('.calculator-item__input');
        const startValue = sumElement.innerText;
        const newValue = +input.value * +this.price;

        this.animateSum(startValue, newValue, sumElement)
    }

    maskValue(value) {
        const valueStr = value.toString();
        if (valueStr.length <= 3) {
            return valueStr
        } else if (valueStr.length <= 6) {
            return `${valueStr.substr(0, valueStr.length - 3)} ${valueStr.substr(valueStr.length - 3, 3)}`
        } else if (valueStr.length > 6) {
            return `${valueStr.substr(0, valueStr.length - 6)} ${valueStr.substr(valueStr.length - 6, valueStr.length - 4)} ${valueStr.substr(valueStr.length - 3, 3)}`
        }
    }

    animateSum(startValue, newValue, sumElement) {
        if (!this.animationFlow) {
            this.animationFlow = true;
            setTimeout(() => {
                this.animationFlow = false;
            }, 250);

            const correctStartValue = +(startValue.split(' ').join(''));
            let sum = +correctStartValue;
            const step = (+newValue - +correctStartValue) / 10;

            const doStep = () => {
                sum += step;
                sumElement.innerHTML = this.maskValue(+sum);
                console.log(this.maskValue(+sum));
                if (sum === +newValue) {
                    return ''
                } else {
                    setTimeout(() => doStep(), 20)
                }
            };
            doStep();
        }
    }


    addEventListeners() {
        this.elem.querySelector('.calculator-item__input-minus').addEventListener('click', this.decrementInput.bind(this));
        this.elem.querySelector('.calculator-item__input-plus').addEventListener('click', this.incrementInput.bind(this));
        this.elem.querySelector('.calculator-item__input').addEventListener('focus', this.onChangeInput.bind(this));
        this.elem.querySelector('.calculator-item__input').addEventListener('blur', this.onBlurInput.bind(this));
        this.elem.querySelector('.calculator-item__input').addEventListener('keypress', this.onKeyPressInput.bind(this));
    }


    render() {
        this.elem = document.createElement('div');
        this.elem.className = `${this.containerClass}__calculator-item calculator-item`;
        this.elem.innerHTML = this.calculatorItemsHTML();

        this.addEventListeners();
        return this.elem;
    }

}


class CalculatorCategory {
    constructor({ title, value}) {
        this.title = title;
        this.value = value;

        this.elem = null;
        this.animationFlow = false;
    }

    calculatorCategoryHTML() {
        return `
        <h3>${this.title}</h3>
        <div class="calculator__category-content"></div>
        <div class="calculator__category-sum">
        <span class="calculator__category-span">Итого:</span> <span class="calculator__category-sum-span">0</span> руб.
        </div>
        `
    }

    animateSum(startValue, newValue, sumElement) {
        if (!this.animationFlow) {

            this.animationFlow = true;
            setTimeout(() => {
                this.animationFlow = false;
            }, 250);

            let sum = +startValue;
            const step = (+newValue - +startValue) / 10;

            const doStep = () => {
                sum += step;
                sumElement.innerHTML = this.maskValue(sum);
                if (sum === +newValue) {
                    return ''
                } else {
                    setTimeout(() => doStep(), 20)
                }
            };
            doStep();
        }
    }

    maskValue(value){
        const valueStr = value.toString();
        if (valueStr.length <= 3) {
            return valueStr
        } else if (valueStr.length <= 6) {
            return `${valueStr.substr(0, valueStr.length - 3)} ${valueStr.substr(valueStr.length-3,  3)}`
        } else if (valueStr.length > 6){
            return `${valueStr.substr(0, valueStr.length - 6)} ${valueStr.substr(valueStr.length - 6, valueStr.length -4)} ${valueStr.substr(valueStr.length-3,  3)}`
        }
    }


    calculateSum() {
        const sumSpan = this.elem.querySelector('.calculator__category-sum-span');
        const startValue = +(sumSpan.innerText.split(' ').join(''));
        let sum = 0;

        const calculatorItems = this.elem.querySelectorAll('.calculator-item');
        calculatorItems.forEach(item => {
            sum +=
                +item.querySelector('.calculator-item__price').innerText *
                +item.querySelector('.calculator-item__input').value;
        });

        this.animateSum(startValue, sum, sumSpan);
    }

    onKeyPressInput () {
        setTimeout(() => this.calculateSum(), 20);
    }

    addEventListeners() {
        this.elem.querySelectorAll('.calculator-item__input-minus').forEach(item => item.addEventListener('click', this.calculateSum.bind(this)));
        this.elem.querySelectorAll('.calculator-item__input-plus').forEach(item => item.addEventListener('click', this.calculateSum.bind(this)));
        this.elem.querySelectorAll('.calculator-item__input').forEach(item => item.addEventListener('keypress', this.onKeyPressInput.bind(this)));
    }

    addItemsTitleCLass(){
        this.elem.querySelector('.calculator-item__price').classList.add('calculator-title');
        this.elem.querySelector('.calculator-item__price').classList.add('calculator-price-title');
        this.elem.querySelector('.calculator-item__input-container').classList.add('calculator-title');
        this.elem.querySelector('.calculator-item__input-container').classList.add('calculator-quantity-title');
        this.elem.querySelector('.calculator-item__sum').classList.add('calculator-title');
        this.elem.querySelector('.calculator-item__sum').classList.add('calculator-sum-title');
    }

    render() {
        this.elem = document.createElement('div');
        this.elem.className = 'calculator-category grid-item';
        this.elem.innerHTML = this.calculatorCategoryHTML();

        const contentContainer = this.elem.querySelector(".calculator__category-content");

        this.value.map(item => contentContainer.append(new CalculatorItem(item).render()));

        this.calculateSum();
        this.addEventListeners();
        this.addItemsTitleCLass();

        return this.elem;
    }
}


// const tabsProps = {
//     title: 'Обои',
//     value: [
//         {
//             title: 'Покраска',
//             price: '150'
//         }, {
//             title: 'Покраска',
//             price: '150'
//         }, {
//             title: 'Покраска',
//             price: '150'
//         }, {
//             title: 'Покраска',
//             price: '50'
//         }, {
//             title: 'Покраска',
//             price: '1450'
//         }
//     ]
// };
//
// const tabsProps1 = {
//     title: 'Another',
//     value: [
//         {
//             title: 'Another',
//             price: '120'
//         }, {
//             title: 'Another',
//             price: '1520'
//         }, {
//             title: 'Another',
//             price: '1540'
//         }, {
//             title: 'Another',
//             price: '10'
//         }, {
//             title: 'Another',
//             price: '140'
//         }
//     ]
// };
//
// // new calculatorCategory(tabsProps).render();
