// import CalculatorItem from "./calculator-item";
class CalculatorItem {
    constructor({title, price}) {
        this.title = title;
        this.price = price;

        this.elem = null;
        this.inputValue = '';

        this.animationFlow = false;
        this.changeInputFlow = false;

    }

    calculatorItemsHTML() {
        return `
        <span class="calculator-item__title">${this.title}</span>
        <span class="calculator-item__price">${this.price}</span>
        
        <div class="calculator-item__input-wrapper">
        <div class="calculator-item__input-minus"> <img src="img/cost-calculator-input-minus.svg" alt=""></div>
        <div class="calculator-item__input-container">
        <input class="calculator-item__input" type="number">
        </div>
        <div class="calculator-item__input-plus"><img src="img/cost-calculator-input-plus.svg" alt=""></div>
        </div>
        
        <span class="calculator-item__sum">0</span>

        `
    }

    incrementInput() {
        if (!this.changeInputFlow) {
            this.changeInputFlow = true;
            setTimeout(() => {
                this.changeInputFlow = false;
            }, 250);
            const input = this.elem.querySelector('.calculator-item__input');
            input.value = +input.value + 1;
            this.calculateSum();
        }
    }

    decrementInput() {
        if (!this.changeInputFlow) {
            this.changeInputFlow = true;
            setTimeout(() => {
                this.changeInputFlow = false;
            }, 250);
            const input = this.elem.querySelector('.calculator-item__input');
            if (+input.value - 1 < 0) {
                input.value = 0;
            } else {
                input.value = +input.value - 1;
            }
            this.calculateSum();
        }
    }

    onChangeInput() {
        const input = this.elem.querySelector('.calculator-item__input');
        this.inputValue = input.value;
        input.value = '';
    }

    onBlurInput() {
        const input = this.elem.querySelector('.calculator-item__input');
        if (input.value === '') {
            input.value = this.inputValue
        }
        this.calculateSum();
    }

    onKeyPressInput ({target}) {
        setTimeout(() =>  this.calculateSum(), 20);
        target.value = target.value.length >= 3 ? target.value.slice(0, 2) : target.value;

    }

    calculateSum() {
        const sumElement = this.elem.querySelector('.calculator-item__sum');
        const input = this.elem.querySelector('.calculator-item__input');
        const startValue = sumElement.innerText;
        const newValue = +input.value * +this.price;

        this.animateSum(startValue, newValue, sumElement)
    }

    maskValue(value) {
        const valueStr = value.toString();
        if (valueStr.length <= 3) {
            return valueStr
        } else if (valueStr.length <= 6) {
            return `${valueStr.substr(0, valueStr.length - 3)} ${valueStr.substr(valueStr.length - 3, 3)}`
        } else if (valueStr.length > 6) {
            return `${valueStr.substr(0, valueStr.length - 6)} ${valueStr.substr(valueStr.length - 6, valueStr.length - 4)} ${valueStr.substr(valueStr.length - 3, 3)}`
        }
    }

    animateSum(startValue, newValue, sumElement) {
        if (!this.animationFlow) {
            this.animationFlow = true;
            setTimeout(() => {
                this.animationFlow = false;
            }, 250);

            const correctStartValue = +(startValue.split(' ').join(''));
            let sum = +correctStartValue;
            const step = (+newValue - +correctStartValue) / 10;

            const doStep = () => {
                sum += step;
                sumElement.innerHTML = this.maskValue(+sum);
                console.log(this.maskValue(+sum));
                if (sum === +newValue) {
                    return ''
                } else {
                    setTimeout(() => doStep(), 20)
                }
            };
            doStep();
        }
    }


    addEventListeners() {
        this.elem.querySelector('.calculator-item__input-minus').addEventListener('click', this.decrementInput.bind(this));
        this.elem.querySelector('.calculator-item__input-plus').addEventListener('click', this.incrementInput.bind(this));
        this.elem.querySelector('.calculator-item__input').addEventListener('focus', this.onChangeInput.bind(this));
        this.elem.querySelector('.calculator-item__input').addEventListener('blur', this.onBlurInput.bind(this));
        this.elem.querySelector('.calculator-item__input').addEventListener('keypress', this.onKeyPressInput.bind(this));
    }


    render() {
        this.elem = document.createElement('div');
        this.elem.className = `${this.containerClass}__calculator-item calculator-item`;
        this.elem.innerHTML = this.calculatorItemsHTML();

        this.addEventListeners();
        return this.elem;
    }

}


class CalculatorCategory {
    constructor({ title, value}) {
        this.title = title;
        this.value = value;

        this.elem = null;
        this.animationFlow = false;
    }

    calculatorCategoryHTML() {
        return `
        <h3>${this.title}</h3>
        <div class="calculator__category-content"></div>
        <div class="calculator__category-sum">
        <span class="calculator__category-span">Итого:</span> <span class="calculator__category-sum-span">0</span> руб.
        </div>
        `
    }

    animateSum(startValue, newValue, sumElement) {
        if (!this.animationFlow) {

            this.animationFlow = true;
            setTimeout(() => {
                this.animationFlow = false;
            }, 250);

            let sum = +startValue;
            const step = (+newValue - +startValue) / 10;

            const doStep = () => {
                sum += step;
                sumElement.innerHTML = this.maskValue(sum);
                if (sum === +newValue) {
                    return ''
                } else {
                    setTimeout(() => doStep(), 20)
                }
            };
            doStep();
        }
    }

    maskValue(value){
        const valueStr = value.toString();
        if (valueStr.length <= 3) {
            return valueStr
        } else if (valueStr.length <= 6) {
            return `${valueStr.substr(0, valueStr.length - 3)} ${valueStr.substr(valueStr.length-3,  3)}`
        } else if (valueStr.length > 6){
            return `${valueStr.substr(0, valueStr.length - 6)} ${valueStr.substr(valueStr.length - 6, valueStr.length -4)} ${valueStr.substr(valueStr.length-3,  3)}`
        }
    }


    calculateSum() {
        const sumSpan = this.elem.querySelector('.calculator__category-sum-span');
        const startValue = +(sumSpan.innerText.split(' ').join(''));
        let sum = 0;

        const calculatorItems = this.elem.querySelectorAll('.calculator-item');
        calculatorItems.forEach(item => {
            sum +=
                +item.querySelector('.calculator-item__price').innerText *
                +item.querySelector('.calculator-item__input').value;
        });

        this.animateSum(startValue, sum, sumSpan);
    }

    onKeyPressInput () {
        setTimeout(() => this.calculateSum(), 20);
    }

    addEventListeners() {
        this.elem.querySelectorAll('.calculator-item__input-minus').forEach(item => item.addEventListener('click', this.calculateSum.bind(this)));
        this.elem.querySelectorAll('.calculator-item__input-plus').forEach(item => item.addEventListener('click', this.calculateSum.bind(this)));
        this.elem.querySelectorAll('.calculator-item__input').forEach(item => item.addEventListener('keypress', this.onKeyPressInput.bind(this)));
    }

    addItemsTitleCLass(){
        this.elem.querySelector('.calculator-item__price').classList.add('calculator-title');
        this.elem.querySelector('.calculator-item__price').classList.add('calculator-price-title');
        this.elem.querySelector('.calculator-item__input-container').classList.add('calculator-title');
        this.elem.querySelector('.calculator-item__input-container').classList.add('calculator-quantity-title');
        this.elem.querySelector('.calculator-item__sum').classList.add('calculator-title');
        this.elem.querySelector('.calculator-item__sum').classList.add('calculator-sum-title');
    }

    render() {
        this.elem = document.createElement('div');
        this.elem.className = 'calculator-category grid-item';
        this.elem.innerHTML = this.calculatorCategoryHTML();

        const contentContainer = this.elem.querySelector(".calculator__category-content");

        this.value.map(item => contentContainer.append(new CalculatorItem(item).render()));

        this.calculateSum();
        this.addEventListeners();
        this.addItemsTitleCLass();

        return this.elem;
    }
}


// const tabsProps = {
//     title: 'Обои',
//     value: [
//         {
//             title: 'Покраска',
//             price: '150'
//         }, {
//             title: 'Покраска',
//             price: '150'
//         }, {
//             title: 'Покраска',
//             price: '150'
//         }, {
//             title: 'Покраска',
//             price: '50'
//         }, {
//             title: 'Покраска',
//             price: '1450'
//         }
//     ]
// };
//
// const tabsProps1 = {
//     title: 'Another',
//     value: [
//         {
//             title: 'Another',
//             price: '120'
//         }, {
//             title: 'Another',
//             price: '1520'
//         }, {
//             title: 'Another',
//             price: '1540'
//         }, {
//             title: 'Another',
//             price: '10'
//         }, {
//             title: 'Another',
//             price: '140'
//         }
//     ]
// };
//
// // new calculatorCategory(tabsProps).render();

// class CalculatorItem {
//     constructor({title, price}) {
//         this.title = title;
//         this.price = price;
//
//         this.elem = null;
//         this.inputValue = '';
//
//         this.animationFlow = false;
//
//     }
//
//     calculatorItemsHTML() {
//         return `
//         <span class="calculator-item__title">${this.title}</span>
//         <span class="calculator-item__price">${this.price}</span>
//
//         <div class="calculator-item__input-wrapper">
//         <div class="calculator-item__input-minus"> <img src="img/cost-calculator-input-minus.svg" alt=""></div>
//         <input class="calculator-item__input" type="number">
//         <div class="calculator-item__input-plus"><img src="img/cost-calculator-input-plus.svg" alt=""></div>
//         </div>
//
//         <span class="calculator-item__sum">0</span>
//
//         `
//     }
//
//     incrementInput() {
//         const input = this.elem.querySelector('.calculator-item__input');
//         input.value = +input.value + 1;
//         this.calculateSum();
//     }
//
//     decrementInput() {
//         const input = this.elem.querySelector('.calculator-item__input');
//         if (+input.value - 1 < 0){
//             input.value = 0;
//         } else {
//             input.value = +input.value - 1;
//         }
//         this.calculateSum();
//     }
//
//     onChangeInput() {
//         const input = this.elem.querySelector('.calculator-item__input');
//         this.inputValue = input.value;
//         input.value = '';
//     }
//
//     onBlurInput() {
//         const input = this.elem.querySelector('.calculator-item__input');
//         if (input.value === '') {
//             input.value = this.inputValue
//         }
//         this.calculateSum();
//     }
//
//     calculateSum() {
//         const sumElement = this.elem.querySelector('.calculator-item__sum');
//         const input = this.elem.querySelector('.calculator-item__input');
//         const startValue = sumElement.innerText;
//         const newValue = +input.value * +this.price;
//
//         this.animateSum(startValue, newValue, sumElement)
//     }
//
//     animateSum(startValue, newValue, sumElement) {
//         if (!this.animationFlow) {
//
//             this.animationFlow = true;
//             setTimeout(() => {
//                 this.animationFlow = false;
//             }, 250);
//
//             let sum = +startValue;
//             const step = (+newValue - +startValue) / 10;
//
//             const doStep = () => {
//                 sum += step;
//                 sumElement.innerHTML = sum;
//                 if (sum === +newValue) {
//                     return ''
//                 } else {
//                     setTimeout(() => doStep(), 20)
//                 }
//             };
//             doStep();
//         }
//     }
//
//
//     addEventListeners() {
//         this.elem.querySelector('.calculator-item__input-minus').addEventListener('click', this.decrementInput.bind(this));
//         this.elem.querySelector('.calculator-item__input-plus').addEventListener('click', this.incrementInput.bind(this));
//         this.elem.querySelector('.calculator-item__input').addEventListener('focus', this.onChangeInput.bind(this));
//         this.elem.querySelector('.calculator-item__input').addEventListener('blur', this.onBlurInput.bind(this));
//     }
//
//
//     render() {
//         console.log('render');
//         const container = document.querySelector(`.${this.containerClass}`);
//
//         this.elem = document.createElement('div');
//         this.elem.className = `${this.containerClass}__calculator-item calculator-item`;
//         this.elem.innerHTML = this.calculatorItemsHTML();
//
//         this.addEventListeners();
//         return this.elem;
//     }
//
// };


// export default CalculatorItem;
const itemProps = {
        containerClass: 'cost-calculator',
        title: 'Покраска',
        price: '150'
    };


// new CalculatorItem(itemProps).render();
class CalculatorTab {
    constructor({text, type, size, category}) {
        this.text = text;
        this.type = type;
        this.size = size;
        this.category = category;

        this.elem = null;
        this.changeFlow = false;
    }

    render() {
        this.elem = document.createElement('div');
        this.elem.className = this.type === 'primary'
            ? 'calculator-tab calculator-tab-primary'
            : 'calculator-tab calculator-tab-secondary';
        this.elem.className += this.size === 'large' ? " calculator-tab-large" : '';
        this.elem.setAttribute('data-category', this.category);
        this.elem.innerHTML = `<span>${this.text}</span>`;
        this.elem.addEventListener('click', () => {
            if (!this.changeFlow) {
                this.changeFlow = true;
                setTimeout(() => {this.changeFlow = false;},200);
            document.querySelectorAll('.calculator-tab').forEach(item => item.classList.remove('active'));
            this.elem.classList.add('active');
        }});
        return this.elem;
    }
}

// const tabsParam = [
//     {
//         text: 'Популярные услуги',
//         type: 'primary',
//         size: 'large',
//         category: 'popular'
//     },{
//         text: 'Стены',
//         type: 'primary',
//         size: '',
//         category: 'walls'
//     },{
//         text: 'Полы',
//         type: 'primary',
//         size: '',
//         category: 'floors'
//     },{
//         text: 'Потолки',
//         type: 'primary',
//         size: '',
//         category: 'ceilings'
//     },{
//         text: 'Черновой ремонт',
//         type: 'primary',
//         size: 'large',
//         category: 'rough'
//     },{
//         text: 'Чистовой ремонт',
//         type: 'primary',
//         size: 'large',
//         category: 'fine'
//     },{
//         text: 'Сантехнические работы',
//         type: 'secondary',
//         size: '',
//         category: 'plumbing'
//     },{
//         text: 'Электромонтажные работы',
//         type: 'secondary',
//         size: '',
//         category: 'electrical'
//     },{
//         text: 'Плиточные работы',
//         type: 'secondary',
//         size: '',
//         category: 'tiling'
//     },{
//         text: 'Гипсокартон',
//         type: 'secondary',
//         size: '',
//         category: 'drywall'
//     },{
//         text: 'Демонтаж и перепланировки',
//         type: 'secondary',
//         size: '',
//         category: 'dismantling'
//     },{
//         text: 'Дизайн интерьера',
//         type: 'secondary',
//         size: '',
//         category: 'design'
//     }
// ];


class Calculator {
    constructor({tabs, categories, mainContainerClass}) {
        this.tabs = tabs;
        this.categories = categories;
        this.mainContainerClass = mainContainerClass;

        this.elem = null;
        this.category = 'popular';
        this.changeFlow = false;

    }

    calculatorHTML() {
        return `
        <div class="calculator-tabs"></div>
        <div class="calculator-content"></div>
        <div class="calculator-footer">
            <div class="calculator-footer__item">
                <span class="calculator-footer__item-title">Сроки выполнения</span>
                <div class="calculator-footer__item-description">
                <span class="calculator-footer__item-value" id="js-calculator-footer__days">0</span>
                <span class="calculator-footer__item-secondary">дней</span>
                </div>
            </div>
            <div class="calculator-footer__item">
                <span class="calculator-footer__item-title">Ваша скидка:</span>
                <div class="calculator-footer__item-description">
                <span class="calculator-footer__item-value" id="js-calculator-footer__discount">0</span>
                <span class="calculator-footer__item-secondary">руб.</span>
                </div>
            </div>
            <div class="calculator-footer__item">
                <span class="calculator-footer__item-title">Материалы:</span>
                <div class="calculator-footer__item-description">
                <span class="calculator-footer__item-value" id="js-calculator-footer__material">0</span>
                <span class="calculator-footer__item-secondary">руб.</span>
                </div>
            </div>
            <div class="calculator-footer__item">
                <span class="calculator-footer__item-title">Стоимость работ:</span>
                <div class="calculator-footer__item-description">
                <span class="calculator-footer__item-value" id="js-calculator-footer__sum">0</span>
                <span class="calculator-footer__item-secondary">руб.</span>
                </div>
            </div>
            
            <div class="calculator-footer__button">КОНСУЛЬТАЦИЯ</div>
        </div>      
        `
    }

    initMasonry() {
        const contentContainerCollection = document.querySelectorAll('.grid');
        contentContainerCollection.forEach(item => {
            const masonryContentContainer = new Masonry(item, {
                    // options
                    itemSelector: '.grid-item',
                    columnWidth: 610
                });
        });

    }

    animateSum(startValue, newValue, sumElement) {
        let i = 1;
        let sum = +startValue;
        const step = Math.floor((+newValue - +startValue) / 10);

        const interval = setInterval(() => {
            i++;
            sum += step;
            sumElement.innerHTML = this.maskValue(sum);
            if (i >= 10) {
                sumElement.innerHTML = this.maskValue(newValue);
                clearInterval(interval);
            }
        }, 20)
    }

    maskValue(value) {
        const valueStr = value.toString();
        if (valueStr.length <= 3) {
            return valueStr
        } else if (valueStr.length <= 6) {
            return `${valueStr.substr(0, valueStr.length - 3)} ${valueStr.substr(valueStr.length - 3, 3)}`
        } else if (valueStr.length > 6) {
            return `${valueStr.substr(0, valueStr.length - 6)} ${valueStr.substr(valueStr.length - 6, valueStr.length - 4)} ${valueStr.substr(valueStr.length - 3, 3)}`
        }
    }

    calculateSum() {
        const sumSpan = this.elem.querySelector('#js-calculator-footer__sum');
        const materialSpan = this.elem.querySelector('#js-calculator-footer__material');
        const discountSpan = this.elem.querySelector('#js-calculator-footer__discount');
        const daySpan = this.elem.querySelector('#js-calculator-footer__days');

        const sumStart = +(sumSpan.innerText.split(' ').join(''));
        const materialStart = +(materialSpan.innerText.split(' ').join(''));
        const discountStart = +(discountSpan.innerText.split(' ').join(''));
        const dayStart = +(daySpan.innerText.split(' ').join(''));

        let sum = 0;

        const calculatorItems = this.elem.querySelectorAll('.calculator-item');
        calculatorItems.forEach(item => {
            sum +=
                +item.querySelector('.calculator-item__price').innerText *
                +item.querySelector('.calculator-item__input').value;
        });

        const sumNew = sum;
        const discountNew = Math.floor(sum * 0.07);
        const materialNew = Math.floor((sum - discountNew) * 0.25);
        const dayNew = Math.floor(sum / 10000);

        this.animateSum(sumStart, sumNew, sumSpan);
        this.animateSum(materialStart, materialNew, materialSpan);
        this.animateSum(discountStart, discountNew, discountSpan);
        this.animateSum(dayStart, dayNew, daySpan);

    }

    onKeyPressInput() {
        setTimeout(() => this.calculateSum(), 20);
    }

    changeCategory({target}) {
        if (!this.changeFlow) {
            this.changeFlow = true;
            setTimeout(() => {
                this.changeFlow = false;
            }, 200);
            const id = target.dataset.category || target.parentNode.dataset.category;

            const contentContainersCollection = document.querySelectorAll(`.calculator-content-wrapper`);
            contentContainersCollection.forEach(item => item.classList.remove('calculator-content-wrapper--active'));

            const activeContainer = this.elem.querySelector(`#calculator-content-${id}`);
            activeContainer.classList.add('calculator-content-wrapper--active');
            this.initMasonry();

            const contentHeight = activeContainer.style.height;
            const mainContentContainer = this.elem.querySelector(`.calculator-content`);
            mainContentContainer.style.height = contentHeight;



        }
    }


    addEventListeners() {
        this.elem.querySelectorAll('.calculator-item__input-minus').forEach(item => item.addEventListener('click', this.calculateSum.bind(this)));
        this.elem.querySelectorAll('.calculator-item__input-plus').forEach(item => item.addEventListener('click', this.calculateSum.bind(this)));
        this.elem.querySelectorAll('.calculator-item__input').forEach(item => item.addEventListener('keypress', this.onKeyPressInput.bind(this)));
        this.elem.querySelectorAll('.calculator-tab').forEach(item => item.addEventListener('click', this.changeCategory.bind(this)));
    }

    renderContent ({id, content}) {
        const container = document.createElement('div');
        container.id = `calculator-content-${id}`;
        container.className = `calculator-content-wrapper grid`;
        content.forEach(item => container.append(new CalculatorCategory(item).render()));
        return container;
    }

    showFirstContent () {
        const firstContainer =  this.elem.querySelector('.calculator-content-wrapper');
        firstContainer.classList.add('calculator-content-wrapper--active');

        const contentContainer = this.elem.querySelector('.calculator-content');
        contentContainer.style.height = firstContainer.style.height;
    }

    render() {
        this.elem = document.createElement('div');
        this.elem.className = 'calculator-main-container';
        this.elem.innerHTML = this.calculatorHTML();

        const tabsContainer = this.elem.querySelector('.calculator-tabs');
        this.tabs.forEach(item => tabsContainer.append(new CalculatorTab(item).render()));
        tabsContainer.firstChild.classList.add('active');

        const contentContainer = this.elem.querySelector('.calculator-content');
        this.categories.forEach(item => contentContainer.append(this.renderContent(item)));

        this.showFirstContent();
        this.addEventListeners();
        return this.elem;
    }

    init() {
        const container = document.querySelector(`.${this.mainContainerClass}`);
        container.append(this.render());
        this.initMasonry();
        this.showFirstContent();

    }
}


const calcProps = {
    mainContainerClass: 'calculator',
    tabs: [
        {
            text: 'Популярные услуги',
            type: 'primary',
            size: 'large',
            category: 'popular'
        }, {
            text: 'Стены',
            type: 'primary',
            size: '',
            category: 'walls'
        }, {
            text: 'Полы',
            type: 'primary',
            size: '',
            category: 'floors'
        }, {
            text: 'Потолки',
            type: 'primary',
            size: '',
            category: 'ceilings'
        }, {
            text: 'Черновой ремонт',
            type: 'primary',
            size: 'large',
            category: 'rough'
        }, {
            text: 'Чистовой ремонт',
            type: 'primary',
            size: 'large',
            category: 'fine'
        }, {
            text: 'Сантехнические работы',
            type: 'secondary',
            size: '',
            category: 'plumbing'
        }, {
            text: 'Электромонтажные работы',
            type: 'secondary',
            size: '',
            category: 'electrical'
        }, {
            text: 'Плиточные работы',
            type: 'secondary',
            size: '',
            category: 'tiling'
        }, {
            text: 'Гипсокартон',
            type: 'secondary',
            size: '',
            category: 'drywall'
        }, {
            text: 'Демонтаж и перепланировки',
            type: 'secondary',
            size: '',
            category: 'dismantling'
        }, {
            text: 'Дизайн интерьера',
            type: 'secondary',
            size: '',
            category: 'design'
        }
    ],
    categories: [
        {
            id: 'popular',
            content: [
                {
                    title: 'Обои',
                    value: [
                        {
                            title: 'Покраска',
                            price: '150'
                        }, {
                            title: 'Покраска',
                            price: '150'
                        }, {
                            title: 'Покраска',
                            price: '150'
                        }, {
                            title: 'Покраска',
                            price: '50'
                        }, {
                            title: 'Покраска',
                            price: '1450'
                        }
                    ]
                }, {
                    title: 'Потолки',
                    value: [
                        {
                            title: 'Потолок',
                            price: '15'
                        }, {
                            title: 'Потолок',
                            price: '1400'
                        }, {
                            title: 'Потолок',
                            price: '150'
                        }, {
                            title: 'Потолок',
                            price: '550'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }
                    ]
                }, {
                    title: 'Еще',
                    value: [
                        {
                            title: 'Потолок',
                            price: '15'
                        }, {
                            title: 'Потолок',
                            price: '1400'
                        }, {
                            title: 'Потолок',
                            price: '150'
                        }, {
                            title: 'Потолок',
                            price: '550'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }, {
                            title: 'Потолок',
                            price: '4300'
                        }, {
                            title: 'Потолок',
                            price: '4320'
                        }
                    ]
                }
            ]
        },
        {
            id: 'walls',
            content: [
                {
                    title: 'Стены',
                    value: [
                        {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Стены',
                            price: '50'
                        }, {
                            title: 'Стены',
                            price: '1450'
                        }
                    ]
                }, {
                    title: 'Стены',
                    value: [
                        {
                            title: 'Стены',
                            price: '15'
                        }, {
                            title: 'Стены',
                            price: '1400'
                        }, {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Потолок',
                            price: '550'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }
                    ]
                }
            ]
        },
        {
            id: 'floors',
            content: [
                {
                    title: 'Floors',
                    value: [
                        {
                            title: 'Floors',
                            price: '150'
                        }, {
                            title: 'Floors',
                            price: '150'
                        }, {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Стены',
                            price: '50'
                        }, {
                            title: 'Стены',
                            price: '1450'
                        }
                    ]
                }, {
                    title: 'Стены',
                    value: [
                        {
                            title: 'Стены',
                            price: '15'
                        }, {
                            title: 'Стены',
                            price: '1400'
                        }, {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Потолок',
                            price: '550'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }
                    ]
                }, {
                    title: 'Стены',
                    value: [
                        {
                            title: 'Стены',
                            price: '15'
                        }, {
                            title: 'Стены',
                            price: '1400'
                        }, {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Потолок',
                            price: '550'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }
                    ]
                }, {
                    title: 'Стены',
                    value: [
                        {
                            title: 'Стены',
                            price: '15'
                        }, {
                            title: 'Стены',
                            price: '1400'
                        }, {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Потолок',
                            price: '550'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }
                    ]
                }, {
                    title: 'Стены',
                    value: [
                        {
                            title: 'Стены',
                            price: '15'
                        }, {
                            title: 'Стены',
                            price: '1400'
                        }, {
                            title: 'Стены',
                            price: '150'
                        }, {
                            title: 'Потолок',
                            price: '550'
                        }, {
                            title: 'Потолок',
                            price: '450'
                        }
                    ]
                }
            ]
        },
    ]
};




    new Calculator(calcProps).init();
