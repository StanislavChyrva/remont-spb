import {Portfolio} from "../../scripts/portfolio.js";

const portfolioParam = {
    header: {
        h1: 'Это портфолио номер 2',
        h2: 'Оно обладает классом',
        description1: '3-х комнатная квартира 159 м²',
        description2: 'Определенный ЖК',
        description3: 'Сроки работ: 21 день',
    },

    rightBar: {
        price1: '12 000',
        price2: '44 000',
        price3: '123 000',
    },

    slider: {
        numberOfImages: 24,
        imgFormat: 'jpg',
        alt: ''
    },

    images: {
        numberOfImages: 10,
        imgFormat: 'jpg',
        alt: ''
    },

    costings: [
        {
            title: 'Первая Смета',
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                   
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr> 
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>                      
                </table>
            `
        },
        {
            title: 'Вторая Смета',
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                   
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                         
                </table>
            `
        },
        {
            title: 'Третья Смета',
            HTML: `
                <table>                
                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>               
                </table>
            `
        },

        {
            title: 'Четвертая Смета',
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                   
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr> 
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>              
                </table>
            `
        }


    ],

    miniSliderCategory: 'oneroom'

};

const portfolioMainContainer = document.getElementById('js-portfolio__main-container');
console.log(portfolioMainContainer);
//Вставляем портфолио
portfolioMainContainer.append(new Portfolio(portfolioParam).render());
