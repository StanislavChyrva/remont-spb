
containerId = 'portfolio__description';

class Component {
    constructor(tag, className, content, containerId) {
        this.tag = tag;
        this.className = className;
        this.content = content;
        this.containerId = containerId;
        this.elem = null;
    }

    render () {
        this.elem = document.createElement(`${this.tag}`);
        this.elem.className = this.className;
        this.elem.innerHTML = this.content;
        return this.elem;
    }

    insert(){
        this.render();
        document.getElementById(`${this.containerId}`).append(this.elem);
    }

}

//
// new Component('h1', '', 'Ремонт 2-х комнатной квартиры в ЖК «Царская столица» «Комфорт» класс', containerId).insert();
// new Component('h2', '', 'Комфорт» класс', containerId).insert();
// new Component('p', '', '2-х комнатная квартира 15 м²', containerId).insert();
// new Component('p', '', 'ЖК «Царская столица», ул.Кременчугская, д.11', containerId).insert();
// new Component('p', '', 'Сроки работ: 90 дней', containerId).insert();

console.log('hello');


const miniSliderParam = {
    studio:[
        {
            imgQuantity: 14,
            projectLink: 'portfolio1',
            projectDescription: 'ЖК "Такой-То"',
            projectArea: 100,
        },
        {
            imgQuantity: 24,
            projectLink: 'portfolio2',
            projectDescription: 'ЖК "Другой, но похожий"',
            projectArea: 130,
        },
        {
            imgQuantity: 18,
            projectLink: 'portfolio3',
            projectDescription: 'ЖК "Совсем другой"',
            projectArea: 10,
        },
        {
            imgQuantity: 14,
            projectLink: 'portfolio1',
            projectDescription: 'ЖК "Такой-То"',
            projectArea: 100,
        },
        {
            imgQuantity: 24,
            projectLink: 'portfolio2',
            projectDescription: 'ЖК "Другой, но похожий"',
            projectArea: 130,
        },
        {
            imgQuantity: 18,
            projectLink: 'portfolio3',
            projectDescription: 'ЖК "Совсем другой"',
            projectArea: 10,
        },
        {
            imgQuantity: 14,
            projectLink: 'portfolio1',
            projectDescription: 'ЖК "Такой-То"',
            projectArea: 100,
        },
        {
            imgQuantity: 24,
            projectLink: 'portfolio2',
            projectDescription: 'ЖК "Другой, но похожий"',
            projectArea: 130,
        },
        {
            imgQuantity: 18,
            projectLink: 'portfolio3',
            projectDescription: 'ЖК "Совсем другой"',
            projectArea: 10,
        },
        {
            imgQuantity: 14,
            projectLink: 'portfolio1',
            projectDescription: 'ЖК "Такой-То"',
            projectArea: 100,
        },
        {
            imgQuantity: 24,
            projectLink: 'portfolio2',
            projectDescription: 'ЖК "Другой, но похожий"',
            projectArea: 130,
        },
        {
            imgQuantity: 18,
            projectLink: 'portfolio3',
            projectDescription: 'ЖК "Совсем другой"',
            projectArea: 10,
        }
        ],
    oneroom:[
        {
            imgQuantity: 2,
            projectLink: 'portfolio',
            projectDescription: 'Руддщ',
            projectArea: 1231,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: 'hello',
            projectArea: 0,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    tworooms:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    threerooms:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    comfort:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    european:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    design:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    bathrooms:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
};

class MiniSLider {
    constructor({containerId, category, numberOfSlider, imgQuantity, projectLink, projectDescription, projectArea, imageFormat = 'jpg'}) {
        this.containerId = containerId;
        this.category = category;
        this.imgQuantity = +imgQuantity;
        this.numberOfSlider = +numberOfSlider;
        this.imageFormat = imageFormat;
        this.projectDescription = projectDescription;
        this.projectArea = projectArea;
        this.projectLink = projectLink;
        this.elem = null;
    }

    render (){
        this.elem = document.createElement('div');
        this.elem.className = `mini-slider__main-container`;
        this.elem.innerHTML = `
          <!-- Add Arrows -->
            <div class="mini-slider__swiper-button-next mini-slider__swiper-button-next-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="img/slider-next-arrow.svg" alt=""></div></div>
            <div class="mini-slider__swiper-button-prev mini-slider__swiper-button-prev-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="img/slider-prev-arrow.svg" alt=""></div></div>

            <div class="mini-slider__main">
                <div class="mini-slider__swiper-container mini-slider__swiper-container-${this.numberOfSlider}">
                    <!-- Additional required wrapper -->
                    <div class="mini-slider__swiper-wrapper swiper-wrapper">
                        <!-- Slides -->
                         ${this.renderImages().join(' ')}

                    </div>
                    <!-- If we need pagination -->
<!--                    <div class="mini-slider__swiper-pagination"></div>-->
                </div>
            </div>
            <a href="${this.projectLink}"><div class="mini-slider__description">
            <span>${this.projectDescription}</span>
            <span>${this.projectArea} м&#178</span>
</div></a>
        `;

       return this.elem;
    }

    swiperInit() {
        let mySwiper = new Swiper(`.mini-slider__swiper-container-${this.numberOfSlider}`, {
            // Optional parameters
            direction: 'horizontal',
            loop: true,

            navigation: {
                nextEl: `.mini-slider__swiper-button-next-${this.numberOfSlider}`,
                prevEl: `.mini-slider__swiper-button-prev-${this.numberOfSlider}`,
            },
        });
    }

    renderImages() {
        const array = [];
        // если изображение в папке больше нуля (Объект параметров.текущая категория.imgCounter[текущий индекс]
        if (this.imgQuantity > 0) {
            for (let i = 1; i <= this.imgQuantity; i++) {
                array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="mini-slider/img/${this.category}/${this.numberOfSlider}/${i}.${this.imageFormat}"></div>`;
            }
        } else {
            array[0] = `<div class="pre-slide"><img class="slide-image" src="mini-slider/img/coming-soon.${this.imageFormat}" alt="Coming Soon"></div>`;
            swiperFraction.display = 'none'; //hide swiper fraction
        }
        return array;
    }

    init(){
        document.getElementById(`${this.containerId}`).append(this.render());
        setTimeout(()=>{
            this.swiperInit();
        },20)


    }

}


function createMiniSliders(category, imageFormat = 'jpg'){
    let i = 1;
    miniSliderParam[category].forEach(element => {
        element.containerId = `mini-slider-container-${i}`;
        element.numberOfSlider = `${i}`;
        element.category = category;
        element.imageFormat = imageFormat;
        new MiniSLider(element).init();
        i++
    });
}

function changeMiniSlider (category, imageFormat = 'jpg') {
    const miniSliderContainers = document.querySelectorAll('.mini-slider-container');
    let counter = 1;
    miniSliderContainers.forEach(container =>
    {

        createPreShowContainer(category, container, counter);
        if (container.querySelector('.mini-slider__main-container')) {
            container.querySelector('.mini-slider__main-container').classList.add('mini-slider__fade-out');
        }
        setTimeout(()=>{
            container.innerHTML = '';
        }, 450);
        counter++;

    });
    setTimeout(()=> {
        createMiniSliders(category, imageFormat);
    }, 500);
}

function createPreShowContainer(category, container, counter, imageFormat = 'jpg'){
    if (+miniSliderParam[category][counter - 1].imgQuantity > 0) {
        container.setAttribute('style', `background: url("mini-slider/img/${category}/${counter}/1.${imageFormat}");`)
    } else {
        container.setAttribute('style', `background: url("mini-slider/img/coming-soon.${imageFormat}");`)

    }
}

function deletePreShowContainers () {
    document.querySelectorAll('.mini-slider__pre-show-container').forEach(elem => elem.remove() );
}
createMiniSliders('studio');

class PortfolioSlider {
    constructor({numberOfImages, imgFormat = 'jpg', alt = ''}) {
        this.numberOfImages = numberOfImages;
        this.imgFormat = imgFormat;
        this.alt = alt;
    }

    render() {
        const swiper = document.createElement('div');
        swiper.classList.add('swiper-container');
        swiper.innerHTML = `
 
    <div class="swiper-wrapper">
            <!-- Slides -->
            ${this.renderImages().join(' ')}
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>
    
    `;
        console.log('swiper return');
        return swiper;
    }

    renderImages() {
        const array = [];
        // если изображение в папке больше нуля (Объект параметров.текущая категория.imgCounter[текущий индекс]
        if (this.numberOfImages > 0) {
            for (let i = 1; i <= this.numberOfImages; i++) {
                array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="img/portfolio-slider/${i}.${this.imgFormat}" alt="${this.alt}"></div>`;
            }
        } else {
            array[0] = `<div class="pre-slide"><img class="slide-image" src="img/portfolio-slider/coming-soon.${this.imgFormat}" alt="Coming Soon"></div>`;
            swiperFraction.display = 'none'; //hide swiper fraction
        }
        return array;
    }

    init() {
        let mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });
    }

}
const portfolioParam = {
    header: {
        h1: 'Ремонт 2-х комнатной квартиры в ЖК «Царская столица» «Комфорт» класс',
        h2: 'Комфорт» класс',
        description1: '2-х комнатная квартира 15 м²',
        description2: 'ЖК «Царская столица», ул.Кременчугская, д.11',
        description3: 'Сроки работ: 90 дней',
    },

    rightBar: {
        price1: '5 000',
        price2: '7 000',
        price3: '6 000',
    },

    slider: {
        numberOfImages: 14,
        imgFormat: 'jpg',
        alt: ''
    },

    images: {
        numberOfImages: 8,
        imgFormat: 'jpg',
        alt: ''
    },

    costings: [
        {
            title: 'Стены',
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                   
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr> 
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>                      
                </table>
            `
        },
        {
            title: 'Полы',
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>               
                </table>
            `
        },
        {
            title: 'Потолки',
            HTML: `
                <table>                
                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>               
                </table>
            `
        }
    ],

    miniSliderCategory: 'comfort'

};


class Portfolio {
    constructor(props) {
        this.props = {...props};
        this.portfolio = null;
        this.contentContainer = null;
    }

    createElem(tag, className, id = '') {
        const elem = document.createElement(`${tag}`);
        elem.className = `${className}`;
        elem.id = `${id}`;

        return elem;
    }

    render() {
        const portfolio = this.createElem('div', 'portfolio', 'portfolio');
        const header = this.createElem('div', 'portfolio__header', 'portfolio__header');
        header.append(this.renderDescription());
        header.append(this.renderRightBar());
        portfolio.append(header);

        portfolio.append(this.renderTabs());

        portfolio.append(this.renderContentContainer());
        portfolio.append(this.renderMiniSliders());
        // createMiniSliders('studio');



        this.portfolio = portfolio;
        return portfolio;
    }


    descriptionHTML() {
        const {h1, h2, description1, description2, description3} = this.props.header;
        const headerHTML = `
        <h1>${h1}</h1>
        <h2>${h2}</h2>
        <p>${description1}</p>
        <p>${description2}</p>
        <p>${description3}</p>
        `;
        return headerHTML;
    }

    renderDescription() {
        const description = this.createElem('div', 'portfolio__description', 'portfolio__description');
        description.innerHTML = this.descriptionHTML();
        return description;
    }

    rightBarHTML() {
        const {price1, price2, price3} = this.props.rightBar;
        const rightBarHTML = `
        <a href="../">Вернуться назад</a>
        <p>Стоимость ремонта: <span>${price1}</span> руб./м²</p>
        <p>Черновые материалы: <span>${price2}</span> руб./м²</p>
        <p>Чистовые материалы: <span>${price3}</span> руб./м²</p>
        
        <div class="portfolio__right-bar__want-the-same-button">Хочу также</div>
        `;

        return rightBarHTML;
    }

    renderRightBar() {
        const rightBar = this.createElem('div', 'portfolio__right-bar', 'portfolio__right-bar');
        rightBar.innerHTML = this.rightBarHTML();

        return rightBar;
    }

    tabsHTML = `
    <div class="portfolio__tab portfolio__tab--active" data-id="1">Все фото объекта</div>
    <div class="portfolio__tab" data-id="2">Смета на ремонт</div>
    <div class="portfolio__tab" data-id="3">Похожие ремонты</div>
    `;

    renderTabs() {
        const tabs = this.createElem('div', 'portfolio__tabs', 'portfolio__tabs');
        tabs.innerHTML = this.tabsHTML;
        tabs.addEventListener('click', this.changeTab);
        return tabs;
    }

    renderContentContainer() {
        this.contentContainer = this.createElem('div', 'portfolio__content-container', 'portfolio__content-container');
        return this.contentContainer;
    }

    contentImagesHTML(){
        const {numberOfImages, imgFormat, alt} = this.props.images;
        const arrayOfHTML = [];
        for (let i = 1; i <= numberOfImages; i++) {
            arrayOfHTML[i - 1] = `<div class="portfolio__image-wrapper"><img class="portfolio__content-image" src="img/portfolio-images/${i}.${imgFormat}" alt="${alt}"></div>`;
        }
        return arrayOfHTML;

    }



    changeTab = ({target}) => {
        if (target.classList.contains('portfolio__tab')) {
            const tabCollection = document.body.querySelectorAll('.portfolio__tab');
            tabCollection.forEach(tab => tab.classList.remove('portfolio__tab--active'));
            target.classList.add('portfolio__tab--active');

            const contentId = target.dataset.id;
            this.showTabContent(contentId);
        }
    };

    showTabContent(tabNumber) {
        if (tabNumber === '1') {
            this.createContent1();
        }

        if (tabNumber === '2') {
            this.createContent2();
        }

        if (tabNumber === '3') {
            this.createContent3();
        }
    }

    createContent1() {
        this.contentContainer.innerHTML = '';

        const sliderContainer = this.createElem('div', 'portfolio__slider-container', 'portfolio__slider-container');
        sliderContainer.innerHTML = ` 
            <!-- Add Arrows -->
            <div class="swiper-button-next"><img src="img/slider-next-arrow.svg" alt=""></div>
            <div class="swiper-button-prev"><img src="img/slider-prev-arrow.svg" alt=""></div>
            `;

        const slider = new PortfolioSlider(this.props.slider);
        sliderContainer.append(slider.render());
        this.contentContainer.append(sliderContainer);
        slider.init();

        const imageContainer = this.createElem('div', 'portfolio__image-container', 'portfolio__image-container');
        imageContainer.innerHTML = this.contentImagesHTML().join(' ');

        this.contentContainer.append(imageContainer);
    }

    renderCostings(){
        const {costings} = this.props;
        const costingsContainer = this.createElem('div', 'portfolio__costings', 'portfolio__costings');
        const costingsTabsContainer =  this.createElem('div', 'portfolio__costings-tabs', 'portfolio__costings-tabs');
        const costingsContentContainer =  this.createElem('div', 'portfolio__costings-contents', 'portfolio__costings-contents');
        let i = 0;
        costings.forEach(({title, HTML}) => {
            const costingsTab = this.createElem('div', 'portfolio__costings-tab');
            costingsTab.dataset.id = `${i}`;
            costingsTab.innerHTML = title;
            costingsTabsContainer.append(costingsTab);

            const costingsContent = this.createElem('div', 'portfolio__costings-content', `portfolio__costings-content-${i}`);
            costingsContent.innerHTML = HTML;
            costingsContent.style.display = 'none';


            const costingsFinalPrice = this.createElem('span', 'portfolio__costings-final-price');
            costingsFinalPrice.innerHTML = `Итого: <span>${this.calculateCostingsPrice(costingsContent)}</span> руб.`;

            costingsContent.querySelector('table').append(costingsFinalPrice);
            costingsContentContainer.append(costingsContent);
            i++;
        });

        costingsTabsContainer.addEventListener('click', this.changeCostings);

        const costingsHeading = this.createElem('h2', '');
        costingsHeading.innerHTML = 'Стоимость ремонта и материалов';

        costingsContainer.append(costingsHeading, costingsTabsContainer, costingsContentContainer);
        return costingsContainer;
    }

    changeCostings = ({target}) => {
        if (target.classList.contains('portfolio__costings-tab')){

            const contentCollection = document.getElementsByClassName('portfolio__costings-content');
              for (let elem of contentCollection) {
                  elem.style.display = `none`;
              }

            const id = target.dataset.id;
            document.getElementById(`portfolio__costings-content-${id}`).style.display = 'unset';

            this.contentContainer.querySelectorAll('.portfolio__costings-tab').forEach(elem => elem.classList.remove('portfolio__costings-tab--active'));
            target.classList.add('portfolio__costings-tab--active');
        }
    };

    calculateCostingsPrice(container){

        let sum = 0;
        const priceCollection = container.querySelectorAll('td.price');
        const array = [];

        function deleteSpaces(string){
            const array = string.split(' ');
            return array.join('');
        }

        priceCollection.forEach(price => sum+= +(deleteSpaces(price.innerText)));
        sum = sum.toString();

        if (sum.length <= 6){
            array.push(sum.substr(0, sum.length - 3));
            array.push(sum.substr(-3, 3));
        }
        if (sum.length >= 7) {
            array.push(sum.substr(0, sum.length - 6));
            array.push(sum.substr(-6, 3));
            array.push(sum.substr(-3, 3));
        }
        return array.join(' ');
    }

    createContent2() {
        this.contentContainer.innerHTML = '';
        this.contentContainer.append(this.renderCostings());

        this.contentContainer.querySelector('.portfolio__costings-tab').classList.add('portfolio__costings-tab--active');
        console.log(this.contentContainer.querySelector('.portfolio__costings-content'));
        this.contentContainer.querySelector('.portfolio__costings-content').style.display = 'unset';

    }

    createContent3(){
        this.contentContainer.innerHTML = '';
    }

    miniSliderHTML = `
    <div class="mini-slider-container" id="mini-slider-container-1"></div>
    <div class="mini-slider-container mini-slider-container--margin" id="mini-slider-container-2"></div>
    <div class="mini-slider-container " id="mini-slider-container-3"></div>
    <div class="mini-slider-container" id="mini-slider-container-4"></div>
    <div class="mini-slider-container mini-slider-container--margin" id="mini-slider-container-5"></div>
    <div class="mini-slider-container" id="mini-slider-container-6"></div>
    <div class="mini-slider-container" id="mini-slider-container-7"></div>
    <div class="mini-slider-container mini-slider-container--margin" id="mini-slider-container-8"></div>
    <div class="mini-slider-container" id="mini-slider-container-9"></div>
    <div class="mini-slider-container" id="mini-slider-container-10"></div>
    <div class="mini-slider-container mini-slider-container--margin" id="mini-slider-container-11"></div>
    <div class="mini-slider-container" id="mini-slider-container-12"></div>
    `;

    renderMiniSliders(){
        const miniSliderContainer = this.createElem('div', 'mini-slider')
        miniSliderContainer.innerHTML = this.miniSliderHTML;

        return miniSliderContainer;
    }


}

document.body.prepend(new Portfolio(portfolioParam).render());
const sliderParam = {
    /*
    Формат изображения {строка, например: "jpg"}
     */
    imageFormat: 'jpg',

    studio: {
        // Количество квадратных метров на кнопке {число}
        apartmentSize: [45, 48, 67, 76, 82, 32, 64, 86, 54, 58, 123, 123, 123],
        // Количество изображений в папке {число}
        imgCounter: [14, 24, 18, 19, 11, 9, 13, 23, 11, 11, 0, 0, 0],
        // Подпись под изображением - категория {строка}
        descriptionCategory: ['12-х комнатная квартира 14 м²', '2-х комнатная квартира 24 м²', '2-х комнатная квартира 18 м²', '12-х комнатная квартира 14 м²', '2-х комнатная квартира 24 м²', '2-х комнатная квартира 18 м²', '12-х комнатная квартира 14 м²', '2-х комнатная квартира 24 м²', '2-х комнатная квартира 18 м²', '12-х комнатная квартира 14 м²', '2-х комнатная квартира 24 м²', '2-х комнатная квартира 18 м²', '2-х комнатная квартира 18 м²'],
        // Подпись под изображением - проект {строка}
        descriptionObject: ['Проспект Энгельса, д.86', 'ЖК «Академ-Парк»', 'ЖК «Кантемировский»', 'Квартира в ЖК «Сириус»', 'ЖК «Палацио»', 'ЖК «GreenЛандия»', 'ЖК «Академ-Парк»', 'ЖК «Граф Орлов»', 'Квартира в ЖК «Паруса»', 'ЖК «Европа Сити»'],
        // Подпись под изображением - цена {строка}
        descriptionPrice: ['5000', '3000', '1000','5000', '3000', '1000','5000', '3000', '1000','5000', '3000', '1000','5000'],
        // Подпись под изображением - материалы {строка}
        descriptionMaterials: ['3000', '2000', '600','3000', '2000', '500','12000', '5000', '7000','900', '3000', '1000','5000'],
        // Уникальный идентификатор страницы проекта {строка}
        projectLink: ['1', '2', '3','4', '5', '6','7', '8', '9','10', '11', '12','13'],
        // Значение аттрибута Alt {строка}
        altAttribute: ['Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия', 'Квартира-студия']
    },
    oneroom: {
        apartmentSize: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
        descriptionCategory: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionObject: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionPrice: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionMaterials: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        projectLink: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        altAttribute: ['1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные', '1-комнатные']
    },

    tworooms: {
        apartmentSize: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
        descriptionCategory: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionObject: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionPrice: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionMaterials: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        projectLink: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        altAttribute: ['', '', '', '', '', '', '', '', '', '', '', '', '']
    },

    threerooms: {
        apartmentSize: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
        descriptionCategory: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionObject: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionPrice: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionMaterials: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        projectLink: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        altAttribute: ['', '', '', '', '', '', '', '', '', '', '', '', '']
    },

    comfort: {
        apartmentSize: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
        descriptionCategory: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionObject: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionPrice: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionMaterials: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        projectLink: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        altAttribute: ['', '', '', '', '', '', '', '', '', '', '', '', '']
    },

    european: {
        apartmentSize: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
        descriptionCategory: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionObject: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionPrice: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionMaterials: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        projectLink: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        altAttribute: ['Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт', 'Евроремонт']
    },

    design: {
        apartmentSize: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
        descriptionCategory: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionObject: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionPrice: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionMaterials: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        projectLink: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        altAttribute: ['Дизайнерский', 'Дизайнерский', 'Дизайнерский', 'Дизайнерский', 'Дизайнерский', 'Дизайнерский', 'Дизайнерский', 'Дизайнерский', 'Дизайнерский', 'Дизайнерский']
    },

    bathrooms: {
        apartmentSize: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
        descriptionCategory: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionObject: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionPrice: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        descriptionMaterials: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        projectLink: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
        altAttribute: ['', '', '', '', '', '', '', '', '', '', '', '', '']
    }
};
const main = document.getElementsByClassName('main')[0];
const holder = document.getElementsByClassName('swiper-wrapper')[0];
const preHolder = document.getElementsByClassName('pre-holder')[0];
const switchCollection = document.getElementsByClassName('swiper-pagination-bullet');
const buttonCollection = document.getElementsByClassName('slider__category__button');
const descriptionCategory = document.getElementsByClassName('slider__project-description-category')[0];
const descriptionObject = document.getElementsByClassName('slider__project-description-object')[0];
const descriptionPrice = document.getElementsByClassName('slider__project-description-price')[0];
const descriptionMaterials = document.getElementsByClassName('slider__project-description-materials')[0];
const projectLink = document.getElementsByClassName('slider__project-description-link')[0];
const swiperFraction = document.getElementsByClassName('swiper-pagination-fraction')[0];
const imgCollection = document.getElementsByClassName('slide-image');
let changeCategoryIdentifier = true;


let categoryGlobal = 'studio';
let identifierGlobal = 1;
slideWidth = document.documentElement.clientWidth;

/**
 * @description Return array with HTML div > img
 * @returns {array}
 */
function renderImages() {
    const array = [];
    // если изображение в папке больше нуля (Объект параметров.текущая категория.imgCounter[текущий индекс]
    if (sliderParam[categoryGlobal]['imgCounter'][identifierGlobal - 1] > 0) {
        for (let i = 1; i <= sliderParam[categoryGlobal]['imgCounter'][identifierGlobal - 1]; i++) {
            array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="touch-slider/img/${categoryGlobal}/${identifierGlobal}/${i}.${sliderParam.imageFormat}" alt="${sliderParam[categoryGlobal].altAttribute[identifierGlobal - 1]}"></div>`;
        }
    } else {
        array[0] = `<div class="pre-slide"><img class="slide-image" src="touch-slider/img/coming-soon.${sliderParam.imageFormat}" alt="Coming Soon"></div>`;
        swiperFraction.display = 'none'; //hide swiper fraction
    }
    return array;
}

/**
 * @description Return swiper DOM element with imgs and pagination
 * @returns {HTMLDivElement}
 */
function renderSwiper() {
    const swiper = document.createElement('div');
    swiper.classList.add('swiper-container');
    swiper.innerHTML = `


    <div class="swiper-wrapper">
            <!-- Slides -->
            ${this.renderImages().join(' ')}
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>
    `;
    return swiper;
}

/**
 * @description Initialization of swiper (instruction https://swiperjs.com/)
 */
function swiperInit() {
    let mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },


    });
}

/**
 * @description Change categoryGlobal and identifierGlobal. Use 'data-category' attr of HTML .slider__category-span.
 * Change text of description category and buttons.
 *
 * @param event.target
 */
function chooseCategory({target}) {
    if (changeCategoryIdentifier) {
    if (target.classList.contains('slider__category-span')) {
        changeCategoryIdentifier = false;
        setTimeout(()=>{changeCategoryIdentifier = true}, 700);
        descriptionCategory.textContent = target.textContent;
        let i = 0;
        categoryGlobal = target.getAttribute('data-category');
        console.log(categoryGlobal);
        identifierGlobal = 1;

        const buttonCollection = document.getElementsByClassName('slider__category__button__span');
        for (element of buttonCollection) {
            element.innerHTML = `${sliderParam[categoryGlobal].apartmentSize[i]} м&#178; `;
            i++;
        }
        changeDescription(identifierGlobal-1);
        switchOnChooses();
        switchZeroingButton();
        changeMiniSlider(categoryGlobal);
    }

    }

}

/**
 * @description Change identifierGlobal and description object text
 * @param event.target
 */
function chooseButton({target}) {
    if (target.classList.contains('slider__category__button')) {
        identifierGlobal = target.getAttribute('data-identifier');
        changeDescription(identifierGlobal - 1);
        // descriptionObject.textContent = sliderParam[categoryGlobal].descriptionObject[identifierGlobal - 1];
        switchOnChooses();
    } else if (target.classList.contains('slider__category__button__span')) {
        identifierGlobal = target.parentNode.getAttribute('data-identifier');
        changeDescription(identifierGlobal - 1);
        // descriptionObject.textContent = sliderParam[categoryGlobal].descriptionObject[identifierGlobal - 1];
        switchOnChooses();
    }
}

/**
 * @description Append div > img (first image or coming-soon) to .pre-holder. Using for smooth transition when change button or category.
 */
function createImagePreHolder() {
    if (sliderParam[categoryGlobal]['imgCounter'][identifierGlobal - 1] > 0) {

        let image = document.createElement('div');
        image.classList.add('pre-slide-wrapper');
        image.innerHTML = `
        <div class="pre-slide"><img class="pre-slide-image" src="touch-slider/img/${categoryGlobal}/${identifierGlobal}/1.${sliderParam.imageFormat}" alt="${sliderParam[categoryGlobal].altAttribute[identifierGlobal - 1]}"></div>
        `;
        preHolder.append(image);


    } else {
        let image = document.createElement('div');
        image.classList.add('pre-slide-wrapper');
        image.innerHTML = `
        <div class="pre-slide"><img class="pre-slide-image" src="touch-slider/img/coming-soon.${sliderParam.imageFormat}" alt="Coming Soon"></div>
        `;
        preHolder.append(image);

    }
}

/**
 * @description Delete swiper and add new swiper with new imgs with smooth transition
 */
function switchOnChooses() {
    preHolder.innerHTML = '';
    createImagePreHolder();

    main.classList.add('transition');
    preHolder.classList.add('transition');

    setTimeout(() => {
        main.classList.add('fade-out');
        preHolder.classList.add('fade-in');
    }, 50);

    setTimeout(() => {
        preHolder.classList.add('forward');
        main.classList.remove('transition');
        main.innerHTML = '';
        setTimeout(() => {
            main.append(renderSwiper());
        }, 10);
        setTimeout(swiperInit, 20);
        setTimeout(() => {
                preHolder.classList.remove('forward');
            }, 40);
    }, 360);
    //
    setTimeout(() => {
        main.classList.remove('fade-out');
    }, 360);

}

/**
 * @description Change active button. Using when choose category
 */
function switchZeroingButton() {
    for (element of buttonCollection) {
        element.classList.remove('slider__category__button--active');
    }
    buttonCollection[0].classList.add('slider__category__button--active');
    descriptionObject.textContent = sliderParam[categoryGlobal].descriptionObject[0];
}

/**
 * @description Add listener
 */
function addListener() {
    document.body.addEventListener('click', chooseCategory);
    document.body.addEventListener('click', chooseButton);
}

/**
 * @description Create and init swiper, change button and category text
 */
function onLoad() {
    let i = 0;
    const buttonCollection = document.getElementsByClassName('slider__category__button__span');
    console.log(sliderParam);
    for (element of buttonCollection) {
        element.innerHTML = `${sliderParam[categoryGlobal].apartmentSize[i]} м&#178; `;
        i++;
    }

    changeDescription(0);
    main.innerHTML = '';
    main.append(renderSwiper());
    setTimeout(swiperInit, 100);
    addListener();

}

/**
 * change description of slider
 * @param identifier {number}
 */
function changeDescription(identifier){
    descriptionObject.textContent = sliderParam[categoryGlobal].descriptionObject[+identifier];
    descriptionCategory.textContent = sliderParam[categoryGlobal].descriptionCategory[+identifier];
    descriptionPrice.textContent = sliderParam[categoryGlobal].descriptionPrice[+identifier];
    descriptionMaterials.textContent = sliderParam[categoryGlobal].descriptionMaterials[+identifier];
    projectLink.href = `./portfolio${sliderParam[categoryGlobal].projectLink[+identifier]}`;
}

document.addEventListener("DOMContentLoaded", onLoad);
// onLoad();


//Контейнер кнопок с категориями
const buttonContainer = document.getElementById('js-slider__category__button-container');


/**
 * @description Функция для listener, добавляет класс "active" для e.target
 * @param e
 */
const onFocusSetButtonClass = function({target}){
    const buttonCollection = document.getElementsByClassName('slider__category__button');
    //Если клик по контейнеру
    if (target.classList.contains('slider__category__button')) {
        target.classList.remove('slider__category__button--transition');

        for (element of buttonCollection) {
            element.classList.remove('slider__category__button--active');
        }
        target.classList.add('slider__category__button--active');
        setTimeout(()=>{
                      target.classList.add('slider__category__button--transition');
        }, 310);
    }
    //Если клик по span
    if (target.classList.contains('slider__category__button__span')) {
        target.classList.remove('slider__category__button--transition');

        for (element of buttonCollection) {
            element.classList.remove('slider__category__button--active');
        }

        target.parentNode.classList.add('slider__category__button--active');
        setTimeout(()=>{

            target.parentNode.classList.add('slider__category__button--transition');
        }, 310);
    }
};

buttonContainer.addEventListener('click', onFocusSetButtonClass);

//Контейнер с категориями
const categoryContainer = document.getElementById('js-slider__category-container');


/**
 * @description Функция для listener, добавляет класс "active" для e.target
 * @param e
 */
const onFocusSetCategoryClass = function({target}){
    if (changeCategoryIdentifier){
    const categoryCollection = document.getElementsByClassName('slider__category-span')
    if (target.classList.contains('slider__category-span')) {
        target.classList.remove('slider__category-container--transition');
        for (element of categoryCollection) {
            element.classList.remove('slider__category-container--active');
        }
        target. classList.add('slider__category-container--active');

        setTimeout(()=>{
            target. classList.add('slider__category-container--transition');

        }, 310);

    }
    }
};

categoryContainer.addEventListener('click', onFocusSetCategoryClass);


// const sliderParam = {
//     //Основные параметры слайдера -> категория {текст в кнопке (* м2) - 10 значений, колличество изображений в папке, подпись проекта под слайдером}
//     imageFormat: 'jpg',
//     standard: {
//         apartmentSize: [32, 42, 46, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [4, 21, 10, 1, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['ЖК "Царская столица"', 'Название проекта', '', '', '', '', '', '', '', ''],
//         altAttribute: ['SomeAltValue', 'AnotherAlt', '', '', '', '', '', '', '', '']
//     },
//     european: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [8, 5, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     overhaul: {
//         apartmentSize: [113, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     design: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     studios: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     oneroom: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     tworooms: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     threerooms: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     fourandmore: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     cottages: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     bathrooms: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     children: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     kitchen: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     }
// };
