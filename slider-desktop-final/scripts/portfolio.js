import {PortfolioSlider} from "./portfolio-slider.js";
import {MiniSLider} from "./mini-slider.js";


class Portfolio {
    constructor(props) {
        this.props = {...props};
        this.portfolio = null;
        this.contentContainer = null;
        this.miniSlider = null;
    }

    miniSliderLoaded = false; // use this param in lazy load of mini-slider. change to "true" when mini-sliders loaded

    createElem(tag, className, id = '') {
        const elem = document.createElement(`${tag}`);
        elem.className = `${className}`;
        elem.id = `${id}`;

        return elem;
    }

    //main render
    render() {
        const portfolio = this.createElem('div', 'portfolio', 'portfolio');
        const header = this.createElem('div', 'portfolio__header', 'portfolio__header');
        header.append(this.renderDescription());
        header.append(this.renderRightBar());
        portfolio.append(header);

        portfolio.append(this.renderTabs());

        portfolio.append(this.renderContentContainer());
        portfolio.append(this.renderMiniSliderCategoryButton());
        portfolio.append(this.renderMiniSliders());

        //lazy load mini-slider function
        window.addEventListener('scroll', this.lazyLoadMiniSlider);

        this.createContent1();


        this.portfolio = portfolio;
        return portfolio;
    }

    lazyLoadMiniSlider = () => {
        let height = window.innerHeight;
      if (!this.miniSliderLoaded) {
        if (pageYOffset >= $(this.miniSlider).offset().top-height*1.4) {
            this.createMiniSliders(this.props.miniSliderCategory);
            this.miniSliderLoaded = true;
        }
      }
    };

    descriptionHTML() {
        const {h1, h2, description1, description2, description3} = this.props.header;
        const headerHTML = `
        <h1>${h1}</h1>
        <h2>${h2}</h2>
        <p>${description1}</p>
        <p>${description2}</p>
        <p>${description3}</p>
        `;
        return headerHTML;
    }

    renderDescription() {
        const description = this.createElem('div', 'portfolio__description', 'portfolio__description');
        description.innerHTML = this.descriptionHTML();
        return description;
    }

    rightBarHTML() {
        const {price1, price2, price3} = this.props.rightBar;
        const rightBarHTML = `
        <a href="../">Вернуться назад</a>
        <p>Стоимость ремонта: <span>${price1}</span> руб./м²</p>
        <p>Черновые материалы: <span>${price2}</span> руб./м²</p>
        <p>Чистовые материалы: <span>${price3}</span> руб./м²</p>
        
        <div class="portfolio__right-bar__want-the-same-button">Хочу также</div>
        `;

        return rightBarHTML;
    }

    renderRightBar() {
        const rightBar = this.createElem('div', 'portfolio__right-bar', 'portfolio__right-bar');
        rightBar.innerHTML = this.rightBarHTML();

        return rightBar;
    }

    tabsHTML = `
    <div class="portfolio__tab portfolio__tab--active" data-id="1">Все фото объекта</div>
    <div class="portfolio__tab" data-id="2">Смета на ремонт</div>
    <div class="portfolio__tab" data-id="3">Похожие ремонты</div>
    `;

    renderTabs() {
        const tabs = this.createElem('div', 'portfolio__tabs', 'portfolio__tabs');
        tabs.innerHTML = this.tabsHTML;
        tabs.addEventListener('click', this.changeTab);
        return tabs;
    }

    renderContentContainer() {
        this.contentContainer = this.createElem('div', 'portfolio__content-container', 'portfolio__content-container');
        return this.contentContainer;
    }

    contentImagesHTML(){
        const {numberOfImages, imgFormat, alt} = this.props.images;
        const arrayOfHTML = [];
        for (let i = 1; i <= numberOfImages; i++) {
            arrayOfHTML[i - 1] = `<div class="portfolio__image-wrapper"><img class="portfolio__content-image" src="img/portfolio-images/${i}.${imgFormat}" alt="${alt}"></div>`;
        }
        return arrayOfHTML;

    }


    //change tabs properties and show content
    changeTab = ({target}) => {
        if (target.classList.contains('portfolio__tab')) {
            const tabCollection = document.body.querySelectorAll('.portfolio__tab');
            tabCollection.forEach(tab => tab.classList.remove('portfolio__tab--active'));
            target.classList.add('portfolio__tab--active');

            const contentId = target.dataset.id;
            this.showTabContent(contentId);
        }
    };

    showTabContent(tabNumber) {
        if (tabNumber === '1') {
            this.createContent1();
        }

        if (tabNumber === '2') {
            this.createContent2();
        }

        if (tabNumber === '3') {
            this.createContent3();
        }
    }

    createContent1() {
        this.contentContainer.innerHTML = '';

        const sliderContainer = this.createElem('div', 'portfolio__slider-container', 'portfolio__slider-container');
        sliderContainer.innerHTML = ` 
            <!-- Add Arrows -->
            <div class="swiper-button-next"><img src="../img/slider-next-arrow.svg" alt=""></div>
            <div class="swiper-button-prev"><img src="../img/slider-prev-arrow.svg" alt=""></div>
            `;

        //init new slider
        const slider = new PortfolioSlider(this.props.slider);
        sliderContainer.append(slider.render());
        this.contentContainer.append(sliderContainer);
        setTimeout(()=>{slider.init();}, 100);

        //create images container
        const imageContainer = this.createElem('div', 'portfolio__image-container', 'portfolio__image-container');
        imageContainer.innerHTML = this.contentImagesHTML().join(' ');

        this.contentContainer.append(imageContainer);
    }

    renderCostings(){
        const {costings} = this.props;
        const costingsContainer = this.createElem('div', 'portfolio__costings', 'portfolio__costings');
        const costingsTabsContainer =  this.createElem('div', 'portfolio__costings-tabs', 'portfolio__costings-tabs');
        const costingsContentContainer =  this.createElem('div', 'portfolio__costings-contents', 'portfolio__costings-contents');
        let i = 0;
        costings.forEach(({title, HTML}) => {
            const costingsTab = this.createElem('div', 'portfolio__costings-tab');
            costingsTab.dataset.id = `${i}`;
            costingsTab.innerHTML = title;
            costingsTabsContainer.append(costingsTab);

            const costingsContent = this.createElem('div', 'portfolio__costings-content', `portfolio__costings-content-${i}`);
            costingsContent.innerHTML = HTML;
            costingsContent.style.display = 'none';


            const costingsFinalPrice = this.createElem('span', 'portfolio__costings-final-price');
            costingsFinalPrice.innerHTML = `Итого: <span>${this.calculateCostingsPrice(costingsContent)}</span> руб.`;

            costingsContent.querySelector('table').append(costingsFinalPrice);
            costingsContentContainer.append(costingsContent);
            i++;
        });

        costingsTabsContainer.addEventListener('click', this.changeCostings);

        const costingsHeading = this.createElem('h2', '');
        costingsHeading.innerHTML = 'Стоимость ремонта и материалов';

        costingsContainer.append(costingsHeading, costingsTabsContainer, costingsContentContainer);
        return costingsContainer;
    }

    //change the properties of costing tabs and show costing
    changeCostings = ({target}) => {
        if (target.classList.contains('portfolio__costings-tab')){

            const contentCollection = document.getElementsByClassName('portfolio__costings-content');
              for (let elem of contentCollection) {
                  elem.style.display = `none`;
              }

            const id = target.dataset.id;
            document.getElementById(`portfolio__costings-content-${id}`).style.display = 'unset';

            this.contentContainer.querySelectorAll('.portfolio__costings-tab').forEach(elem => elem.classList.remove('portfolio__costings-tab--active'));
            target.classList.add('portfolio__costings-tab--active');
        }
    };

    // find all <td class='price'> and sum innerText, splits by three characters
    calculateCostingsPrice(container){

        let sum = 0;
        const priceCollection = container.querySelectorAll('td.price');
        const array = [];

        function deleteSpaces(string){
            const array = string.split(' ');
            return array.join('');
        }

        priceCollection.forEach(price => sum+= +(deleteSpaces(price.innerText)));
        sum = sum.toString();

        if (sum.length <= 6){
            array.push(sum.substr(0, sum.length - 3));
            array.push(sum.substr(-3, 3));
        }
        if (sum.length >= 7) {
            array.push(sum.substr(0, sum.length - 6));
            array.push(sum.substr(-6, 3));
            array.push(sum.substr(-3, 3));
        }
        return array.join(' ');
    }

    createContent2() {
        this.contentContainer.innerHTML = '';
        this.contentContainer.append(this.renderCostings());

        this.contentContainer.querySelector('.portfolio__costings-tab').classList.add('portfolio__costings-tab--active');
        console.log(this.contentContainer.querySelector('.portfolio__costings-content'));
        this.contentContainer.querySelector('.portfolio__costings-content').style.display = 'unset';


        const imageContainer = this.createElem('div', 'portfolio__image-container', 'portfolio__image-container');
        imageContainer.innerHTML = this.contentImagesHTML().join(' ');

        this.contentContainer.append(imageContainer);

    }

    createContent3(){
        this.contentContainer.innerHTML = '';
    }

    miniSliderHTML = `
    <div class="mini-slider-container" id="mini-slider-container-1"></div>
    <div class="mini-slider-container mini-slider-container--margin" id="mini-slider-container-2"></div>
    <div class="mini-slider-container " id="mini-slider-container-3"></div>
    <div class="mini-slider-container" id="mini-slider-container-4"></div>
    <div class="mini-slider-container mini-slider-container--margin" id="mini-slider-container-5"></div>
    <div class="mini-slider-container" id="mini-slider-container-6"></div>
    <div class="mini-slider-container" id="mini-slider-container-7"></div>
    <div class="mini-slider-container mini-slider-container--margin" id="mini-slider-container-8"></div>
    <div class="mini-slider-container" id="mini-slider-container-9"></div>
    <div class="mini-slider-container" id="mini-slider-container-10"></div>
    <div class="mini-slider-container mini-slider-container--margin" id="mini-slider-container-11"></div>
    <div class="mini-slider-container" id="mini-slider-container-12"></div>
    `;

    renderMiniSliders(){
        const miniSliderContainer = this.createElem('div', 'mini-slider', 'mini-slider');
        miniSliderContainer.innerHTML = this.miniSliderHTML;

        this.miniSlider = miniSliderContainer;
        return miniSliderContainer;
    }

    createMiniSliders(category, imageFormat = 'jpg', portfolio = true){
        let i = 1;
        miniSliderParam[category].forEach(element => {
            element.containerId = `mini-slider-container-${i}`;
            element.numberOfSlider = `${i}`;
            element.category = category;
            element.imageFormat = imageFormat;
            new MiniSLider(element, portfolio).init();
            i++;
        });
    }

    miniSliderCategoryButtonHTML = `
     <div class="slider__category-container" id="js-slider__category-container">               
            <span data-category="studio"
                  class="slider__category-span">Квартиры-студии</span>
                <span data-category="oneroom" class="slider__category-span">1-комнатные</span>
                <span data-category="tworooms" class="slider__category-span">2-х комнатные</span>
                <span data-category="threerooms" class="slider__category-span">3 и более комнат</span>
                <span data-category="comfort" class="slider__category-span">Комфорт</span>
                <span data-category="european" class="slider__category-span">Евроремонт</span>
                <span data-category="design" class="slider__category-span">Дизайнерский</span>
                <span data-category="bathrooms" class="slider__category-span">Ванные комнаты</span>
            </div>
    `;

    renderMiniSliderCategoryButton(){
        const miniSliderCategory = this.createElem('div', 'slider__category-container', 'js-slider__category-container');
        miniSliderCategory.innerHTML = this.miniSliderCategoryButtonHTML;
        miniSliderCategory.querySelector(`[data-category="${this.props.miniSliderCategory}"]`).classList.add('slider__category-container--active');
        miniSliderCategory.addEventListener('click', this.changeMiniSliderCategory);

        return miniSliderCategory;
    }

    changeMiniSliderCategory = ({target}) => {
        console.log(target);
        if (target.classList.contains('slider__category-span')) {

            target.parentNode.childNodes.forEach(elem => elem.className = 'slider__category-span');
            target.classList.add('slider__category-container--active');

            const category = target.getAttribute('data-category');
            this.changeMiniSlider(category);

        }
    };

    changeMiniSlider(category, imageFormat = 'jpg') {
        const miniSliderContainers = document.querySelectorAll('.mini-slider-container');
        let counter = 1;
        miniSliderContainers.forEach(container => {
            this.createPreShowContainer(category, container, counter);
            if (container.querySelector('.mini-slider__main-container')) {
                container.querySelector('.mini-slider__main-container').classList.add('mini-slider__fade-out');
            }
            counter++;
        });

        setTimeout(() => {
            console.log('create new ms');
            this.createMiniSliders(category, imageFormat);
        }, 50);

        setTimeout(()=>{
            console.log('del preshow');
            miniSliderContainers.forEach(container => container.innerHTML = '');
        }, 450);

    }

    createPreShowContainer(category, container, counter, imageFormat = 'jpg') {
        if (+miniSliderParam[category][counter - 1].imgQuantity > 0) {
            container.setAttribute('style', `background: url("../mini-slider/img/${category}/${counter}/1.${imageFormat}");`)
        } else {
            container.setAttribute('style', `background: url("../mini-slider/img/coming-soon.${imageFormat}");`)

        }
    }

}

export {Portfolio};

//ПРИМЕР ОБЪЕКТА ПРАМЕТРОВ
// const portfolioParam = {
//     header: {
//         h1: 'Ремонт 2-х комнатной квартиры в ЖК «Царская столица» «Комфорт» класс',
//         h2: 'Комфорт» класс',
//         description1: '2-х комнатная квартира 15 м²',
//         description2: 'ЖК «Царская столица», ул.Кременчугская, д.11',
//         description3: 'Сроки работ: 90 дней',
//     },
//
//     rightBar: {
//         price1: '5 000',
//         price2: '7 000',
//         price3: '6 000',
//     },
//
//     slider: {
//         numberOfImages: 14,
//         imgFormat: 'jpg',
//         alt: ''
//     },
//
//     images: {
//         numberOfImages: 8,
//         imgFormat: 'jpg',
//         alt: ''
//     },
//
//     costings: [
//         {
//             title: 'Стены',
//             HTML: `
//                 <table>
//                 <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//                 </table>
//             `
//         },
//         {
//             title: 'Полы',
//             HTML: `
//                 <table>
//                 <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//                 <tr><td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//                 <tr><td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//                 <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//                 <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//                 </table>
//             `
//         },
//         {
//             title: 'Потолки',
//             HTML: `
//                 <table>
//                  <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//
//                 <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
//                 <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
//                 </table>
//             `
//         }
//     ],
//
//     miniSliderCategory: 'studio'
//
// };
