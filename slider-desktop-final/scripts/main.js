import {createMiniSliders} from "./mini-slider.js";
import {onLoad} from "./touch-slider.js";

// init touch-slider
document.addEventListener("DOMContentLoaded", onLoad);

// init mini-sliders lazy load
let miniSliderLoaded = false; //true if mini slider loaded
const miniSliderContainer = document.getElementById('mini-slider');
function lazyLoadMiniSlider(){
    let height = window.innerHeight;
    if (!miniSliderLoaded) {
        if (pageYOffset >= $(miniSliderContainer).offset().top-height*1.4) {
            createMiniSliders('studio');
           miniSliderLoaded = true;
        }
    }
}

window.addEventListener('scroll', lazyLoadMiniSlider);



function initCheapSwiper(){
let cheapSwiper = new Swiper('#remont_deshevle__swiper-container', {
    slidesPerView: 6,
    slidesPerColumn: 2,

    spaceBetween: 30,
    loop: true,
    navigation: {
        nextEl: '#remont_deshevle__slider .swiper-container .swiper-button-next',
        prevEl: '#remont_deshevle__slider .swiper-container .swiper-button-prev',
    },
});
}

setTimeout(()=> {
    initCheapSwiper()
},2000);