import {changeMiniSlider} from "./mini-slider.js";

const main = document.getElementsByClassName('main')[0];
const holder = document.getElementsByClassName('swiper-wrapper')[0];
const preHolder = document.getElementsByClassName('pre-holder')[0];
const switchCollection = document.getElementsByClassName('swiper-pagination-bullet');
const buttonCollection = document.getElementsByClassName('slider__category__button');
const descriptionCategory = document.getElementsByClassName('slider__project-description-category')[0];
const descriptionObject = document.getElementsByClassName('slider__project-description-object')[0];
const descriptionPrice = document.getElementsByClassName('slider__project-description-price')[0];
const descriptionMaterials = document.getElementsByClassName('slider__project-description-materials')[0];
const projectLink = document.getElementsByClassName('slider__project-description-link')[0];
const swiperFraction = document.getElementsByClassName('swiper-pagination-fraction')[0];
const imgCollection = document.getElementsByClassName('slide-image');
let changeCategoryIdentifier = true;


let categoryGlobal = 'studio';
let identifierGlobal = 1;
let slideWidth = document.documentElement.clientWidth;

/**
 * @description Return array with HTML div > img
 * @returns {array}
 */
function renderImages() {
    const array = [];
    // если изображение в папке больше нуля (Объект параметров.текущая категория.imgCounter[текущий индекс]
    if (sliderParam[categoryGlobal]['imgCounter'][identifierGlobal - 1] > 0) {
        for (let i = 1; i <= sliderParam[categoryGlobal]['imgCounter'][identifierGlobal - 1]; i++) {
            array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="touch-slider/img/${categoryGlobal}/${identifierGlobal}/${i}.${sliderParam.imageFormat}" alt="${sliderParam[categoryGlobal].altAttribute[identifierGlobal - 1]}"></div>`;
        }
    } else {
        array[0] = `<div class="pre-slide"><img class="slide-image" src="touch-slider/img/coming-soon.${sliderParam.imageFormat}" alt="Coming Soon"></div>`;
           swiperFraction.display = 'none'; //hide swiper fraction
    }
    return array;
}

/**
 * @description Return swiper DOM element with imgs and pagination
 * @returns {HTMLDivElement}
 */
function renderSwiper() {
    const swiper = document.createElement('div');
    swiper.classList.add('swiper-container');
    swiper.innerHTML = `


    <div class="swiper-wrapper">
            <!-- Slides -->
            ${renderImages().join(' ')}
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>
    `;
    return swiper;
}

/**
 * @description Initialization of swiper (instruction https://swiperjs.com/)
 */
function swiperInit() {
    let mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },


    });
}

/**
 * @description Change categoryGlobal and identifierGlobal. Use 'data-category' attr of HTML .slider__category-span.
 * Change text of description category and buttons.
 *
 * @param event.target
 */
function chooseCategory({target}) {
    if (changeCategoryIdentifier) {
    if (target.classList.contains('slider__category-span')) {
        changeCategoryIdentifier = false;
        setTimeout(()=>{changeCategoryIdentifier = true}, 700);
        descriptionCategory.textContent = target.textContent;
        let i = 0;
        categoryGlobal = target.getAttribute('data-category');
        console.log(categoryGlobal);
        identifierGlobal = 1;

        const buttonCollection = document.getElementsByClassName('slider__category__button__span');
        for (let element of buttonCollection) {
            element.innerHTML = `${sliderParam[categoryGlobal].apartmentSize[i]} м&#178; `;
            i++;
        }
        changeDescription(identifierGlobal-1);
        switchOnChooses();
        switchZeroingButton();
        changeMiniSlider(categoryGlobal);
    }

    }

}

/**
 * @description Change identifierGlobal and description object text
 * @param event.target
 */
function chooseButton({target}) {
    if (target.classList.contains('slider__category__button')) {
        identifierGlobal = target.getAttribute('data-identifier');
        changeDescription(identifierGlobal - 1);
        // descriptionObject.textContent = sliderParam[categoryGlobal].descriptionObject[identifierGlobal - 1];
        switchOnChooses();
    } else if (target.classList.contains('slider__category__button__span')) {
        identifierGlobal = target.parentNode.getAttribute('data-identifier');
        changeDescription(identifierGlobal - 1);
        // descriptionObject.textContent = sliderParam[categoryGlobal].descriptionObject[identifierGlobal - 1];
        switchOnChooses();
    }
}

/**
 * @description Append div > img (first image or coming-soon) to .pre-holder. Using for smooth transition when change button or category.
 */
function createImagePreHolder() {
    if (sliderParam[categoryGlobal]['imgCounter'][identifierGlobal - 1] > 0) {

        let image = document.createElement('div');
        image.classList.add('pre-slide-wrapper');
        image.innerHTML = `
        <div class="pre-slide"><img class="pre-slide-image" src="touch-slider/img/${categoryGlobal}/${identifierGlobal}/1.${sliderParam.imageFormat}" alt="${sliderParam[categoryGlobal].altAttribute[identifierGlobal - 1]}"></div>
        `;
        preHolder.append(image);


    } else {
        let image = document.createElement('div');
        image.classList.add('pre-slide-wrapper');
        image.innerHTML = `
        <div class="pre-slide"><img class="pre-slide-image" src="touch-slider/img/coming-soon.${sliderParam.imageFormat}" alt="Coming Soon"></div>
        `;
        preHolder.append(image);

    }
}

/**
 * @description Delete swiper and add new swiper with new imgs with smooth transition
 */
function switchOnChooses() {
    preHolder.innerHTML = '';
    createImagePreHolder();

    main.classList.add('transition');
    preHolder.classList.add('transition');

    setTimeout(() => {
        main.classList.add('fade-out');
        preHolder.classList.add('fade-in');
    }, 50);

    setTimeout(() => {
        preHolder.classList.add('forward');
        main.classList.remove('transition');
        main.innerHTML = '';
        setTimeout(() => {
            main.append(renderSwiper());
        }, 10);
        setTimeout(swiperInit, 20);
        setTimeout(() => {
                preHolder.classList.remove('forward');
            }, 40);
    }, 360);
    //
    setTimeout(() => {
        main.classList.remove('fade-out');
    }, 360);

}

/**
 * @description Change active button. Using when choose category
 */
function switchZeroingButton() {
    for (let element of buttonCollection) {
        element.classList.remove('slider__category__button--active');
    }
    buttonCollection[0].classList.add('slider__category__button--active');
    descriptionObject.textContent = sliderParam[categoryGlobal].descriptionObject[0];
}

/**
 * @description Add listener
 */
function addListener() {
    document.body.addEventListener('click', chooseCategory);
    document.body.addEventListener('click', chooseButton);
}

/**
 * @description Create and init swiper, change button and category text
 */
function onLoad() {
    let i = 0;
    const buttonCollection = document.getElementsByClassName('slider__category__button__span');
    console.log(sliderParam);
    for (let element of buttonCollection) {
        element.innerHTML = `${sliderParam[categoryGlobal].apartmentSize[i]} м&#178; `;
        i++;
    }

    changeDescription(0);
    main.innerHTML = '';
    main.append(renderSwiper());
    setTimeout(swiperInit, 100);
    addListener();

}

/**
 * change description of slider
 * @param identifier {number}
 */
function changeDescription(identifier){
    descriptionObject.textContent = sliderParam[categoryGlobal].descriptionObject[+identifier];
    descriptionCategory.textContent = sliderParam[categoryGlobal].descriptionCategory[+identifier];
    descriptionPrice.textContent = sliderParam[categoryGlobal].descriptionPrice[+identifier];
    descriptionMaterials.textContent = sliderParam[categoryGlobal].descriptionMaterials[+identifier];
    projectLink.href = `./portfolio${sliderParam[categoryGlobal].projectLink[+identifier]}`;
}


// onLoad();


//Контейнер кнопок с категориями
const buttonContainer = document.getElementById('js-slider__category__button-container');


/**
 * @description Функция для listener, добавляет класс "active" для e.target
 * @param e
 */
const onFocusSetButtonClass = function({target}){
    const buttonCollection = document.getElementsByClassName('slider__category__button');
    //Если клик по контейнеру
    if (target.classList.contains('slider__category__button')) {
        target.classList.remove('slider__category__button--transition');

        for (let element of buttonCollection) {
            element.classList.remove('slider__category__button--active');
        }
        target.classList.add('slider__category__button--active');
        setTimeout(()=>{
                      target.classList.add('slider__category__button--transition');
        }, 310);
    }
    //Если клик по span
    if (target.classList.contains('slider__category__button__span')) {
        target.classList.remove('slider__category__button--transition');

        for (let element of buttonCollection) {
            element.classList.remove('slider__category__button--active');
        }

        target.parentNode.classList.add('slider__category__button--active');
        setTimeout(()=>{

            target.parentNode.classList.add('slider__category__button--transition');
        }, 310);
    }
};

buttonContainer.addEventListener('click', onFocusSetButtonClass);

//Контейнер с категориями
const categoryContainer = document.getElementById('js-slider__category-container');


/**
 * @description Функция для listener, добавляет класс "active" для e.target
 * @param e
 */
const onFocusSetCategoryClass = function({target}){
    if (changeCategoryIdentifier){
    const categoryCollection = document.getElementsByClassName('slider__category-span')
    if (target.classList.contains('slider__category-span')) {
        target.classList.remove('slider__category-container--transition');
        for (let element of categoryCollection) {
            element.classList.remove('slider__category-container--active');
        }
        target. classList.add('slider__category-container--active');

        setTimeout(()=>{
            target. classList.add('slider__category-container--transition');

        }, 310);

    }
    }
};

categoryContainer.addEventListener('click', onFocusSetCategoryClass);

export {onLoad};

// const sliderParam = {
//     //Основные параметры слайдера -> категория {текст в кнопке (* м2) - 10 значений, колличество изображений в папке, подпись проекта под слайдером}
//     imageFormat: 'jpg',
//     standard: {
//         apartmentSize: [32, 42, 46, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [4, 21, 10, 1, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['ЖК "Царская столица"', 'Название проекта', '', '', '', '', '', '', '', ''],
//         altAttribute: ['SomeAltValue', 'AnotherAlt', '', '', '', '', '', '', '', '']
//     },
//     european: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [8, 5, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     overhaul: {
//         apartmentSize: [113, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     design: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     studios: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     oneroom: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     tworooms: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     threerooms: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     fourandmore: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     cottages: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     bathrooms: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     children: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     },
//     kitchen: {
//         apartmentSize: [123, 145, 154, 54, 65, 72, 75, 82, 96, 110],
//         imgCounter: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//         descriptionObject: ['', '', '', '', '', '', '', '', '', ''],
//         altAttribute: ['', '', '', '', '', '', '', '', '', '']
//     }
// };

