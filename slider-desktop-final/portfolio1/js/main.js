console.log('hello');
import {Portfolio} from "../../scripts/portfolio.js";

const portfolioParam = {
    // данные для верхнего заголовка
    header: {
        h1: 'Ремонт 2-х комнатной квартиры в ЖК «Царская столица» «Комфорт» класс',
        h2: 'Комфорт» класс',
        description1: '2-х комнатная квартира 15 м²',
        description2: 'ЖК «Царская столица», ул.Кременчугская, д.11',
        description3: 'Сроки работ: 90 дней',
    },

    //данные для правой части хедера с ценами
    rightBar: {
        price1: '5 000',
        price2: '7 000',
        price3: '6 000',
    },

    // параметры для слайдера
    slider: {
        //колличество изображений
        numberOfImages: 14,
        //формат изображений
        imgFormat: 'jpg',
        //каттрибут alt
        alt: ''
    },

    // параметры для уникальных изображений
    images: {
        //колличество изображений
        numberOfImages: 8,
        //формат изображений
        imgFormat: 'jpg',
        //аттрибут alt
        alt: ''
    },

    //массив объектов с HTML разметкой смет
    costings: [
        {
            // заголовок - будет сверстан в кнопку tab на панеле переключения сметы
            title: 'Стены',
            // HTML сверстана через <table>. td.title - большей ширины, там название работы, td.price - ячека с ценой,
            // с нее будет забираться значени при подсчете обзей суммы.
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                   
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr> 
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>                      
                </table>
            `
        },
        {
            title: 'Полы',
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>               
                </table>
            `
        },
        {
            title: 'Потолки',
            HTML: `
                <table>                
                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>               
                </table>
            `
        }
    ],

    //название категории для мини-слайдеров (по названию папок в каталоге mini-slider/img)
    miniSliderCategory: 'studio'

};


const portfolioMainContainer = document.getElementById('js-portfolio__main-container');
console.log(portfolioMainContainer);
//Вставляем портфолио
portfolioMainContainer.append(new Portfolio(portfolioParam).render());


function initCheapSwiper(){
    let cheapSwiper = new Swiper('#remont_deshevle__swiper-container', {
        slidesPerView: 6,
        slidesPerColumn: 2,

        spaceBetween: 30,
        loop: true,
        navigation: {
            nextEl: '#remont_deshevle__slider .swiper-container .swiper-button-next',
            prevEl: '#remont_deshevle__slider .swiper-container .swiper-button-prev',
        },
    });
}

window.addEventListener('load', () => {
    console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    initCheapSwiper()
}, false);

setTimeout(()=> {

},2000);