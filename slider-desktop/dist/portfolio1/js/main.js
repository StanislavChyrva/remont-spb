import {Portfolio} from "../../scripts/portfolio.js";

const portfolioParam = {
    header: {
        h1: 'Ремонт 2-х комнатной квартиры в ЖК «Царская столица» «Комфорт» класс',
        h2: 'Комфорт» класс',
        description1: '2-х комнатная квартира 15 м²',
        description2: 'ЖК «Царская столица», ул.Кременчугская, д.11',
        description3: 'Сроки работ: 90 дней',
    },

    rightBar: {
        price1: '5 000',
        price2: '7 000',
        price3: '6 000',
    },

    slider: {
        numberOfImages: 14,
        imgFormat: 'jpg',
        alt: ''
    },

    images: {
        numberOfImages: 8,
        imgFormat: 'jpg',
        alt: ''
    },

    costings: [
        {
            title: 'Стены',
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                   
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr> 
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>                      
                </table>
            `
        },
        {
            title: 'Полы',
            HTML: `
                <table>                
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>               
                </table>
            `
        },
        {
            title: 'Потолки',
            HTML: `
                <table>                
                 <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>  
                 
                <tr><td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Шпатлёвка стен</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка смесителя</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Установка унитаза</td><td>150</td><td>23</td><td class="price">4 450</td></tr>
                
                <tr><td class="title">Установка раковины</td><td>150</td><td>23</td><td class="price">4 450</td>
                <td class="title">Штукатурка стен под маяк</td><td>150</td><td>23</td><td class="price">4 450</td></tr>               
                </table>
            `
        }
    ],

    miniSliderCategory: 'studio'

};


document.body.prepend(new Portfolio(portfolioParam).render());