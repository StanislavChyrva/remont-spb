
const miniSliderParam = {
    studio:[
        {
            imgQuantity: 14,
            projectLink: 'portfolio1',
            projectDescription: 'ЖК "Такой-То"',
            projectArea: 100,
        },
        {
            imgQuantity: 24,
            projectLink: 'portfolio2',
            projectDescription: 'ЖК "Другой, но похожий"',
            projectArea: 130,
        },
        {
            imgQuantity: 18,
            projectLink: 'portfolio3',
            projectDescription: 'ЖК "Совсем другой"',
            projectArea: 10,
        },
        {
            imgQuantity: 14,
            projectLink: 'portfolio1',
            projectDescription: 'ЖК "Такой-То"',
            projectArea: 100,
        },
        {
            imgQuantity: 24,
            projectLink: 'portfolio2',
            projectDescription: 'ЖК "Другой, но похожий"',
            projectArea: 130,
        },
        {
            imgQuantity: 18,
            projectLink: 'portfolio3',
            projectDescription: 'ЖК "Совсем другой"',
            projectArea: 10,
        },
        {
            imgQuantity: 14,
            projectLink: 'portfolio1',
            projectDescription: 'ЖК "Такой-То"',
            projectArea: 100,
        },
        {
            imgQuantity: 24,
            projectLink: 'portfolio2',
            projectDescription: 'ЖК "Другой, но похожий"',
            projectArea: 130,
        },
        {
            imgQuantity: 18,
            projectLink: 'portfolio3',
            projectDescription: 'ЖК "Совсем другой"',
            projectArea: 10,
        },
        {
            imgQuantity: 14,
            projectLink: 'portfolio1',
            projectDescription: 'ЖК "Такой-То"',
            projectArea: 100,
        },
        {
            imgQuantity: 24,
            projectLink: 'portfolio2',
            projectDescription: 'ЖК "Другой, но похожий"',
            projectArea: 130,
        },
        {
            imgQuantity: 18,
            projectLink: 'portfolio3',
            projectDescription: 'ЖК "Совсем другой"',
            projectArea: 10,
        }
    ],
    oneroom:[
        {
            imgQuantity: 2,
            projectLink: 'portfolio',
            projectDescription: 'Руддщ',
            projectArea: 1231,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: 'hello',
            projectArea: 0,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 4,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    tworooms:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    threerooms:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    comfort:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    european:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    design:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
    bathrooms:[
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        },
        {
            imgQuantity: 0,
            projectLink: 'portfolio',
            projectDescription: '',
            projectArea: 0,
        }
    ],
};

function ReturnParam(paramObject){
    return paramObject;
}

