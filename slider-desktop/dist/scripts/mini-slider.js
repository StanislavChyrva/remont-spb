class MiniSLider {
    constructor({containerId, category, numberOfSlider, imgQuantity, projectLink, projectDescription, projectArea, imageFormat = 'jpg'}, portfolio = false) {
        this.containerId = containerId;
        this.category = category;
        this.imgQuantity = +imgQuantity;
        this.numberOfSlider = +numberOfSlider;
        this.imageFormat = imageFormat;
        this.projectDescription = projectDescription;
        this.projectArea = projectArea;
        this.projectLink = projectLink;
        this.portfolio = portfolio;
        this.elem = null;
    }

    render() {
        this.elem = document.createElement('div');
        this.elem.className = `mini-slider__main-container`;
        if (portfolio) {
            this.elem.innerHTML = `
          <!-- Add Arrows -->
            <div class="mini-slider__swiper-button-next mini-slider__swiper-button-next-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="../img/slider-next-arrow.svg" alt=""></div></div>
            <div class="mini-slider__swiper-button-prev mini-slider__swiper-button-prev-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="../img/slider-prev-arrow.svg" alt=""></div></div>

            <div class="mini-slider__main">
                <div class="mini-slider__swiper-container mini-slider__swiper-container-${this.numberOfSlider}">
                    <!-- Additional required wrapper -->
                    <div class="mini-slider__swiper-wrapper swiper-wrapper">
                        <!-- Slides -->
                         ${this.renderImages().join(' ')}

                    </div>
                    <!-- If we need pagination -->
<!--                    <div class="mini-slider__swiper-pagination"></div>-->
                </div>
            </div>
            <a href="${this.projectLink}"><div class="mini-slider__description">
            <span>${this.projectDescription}</span>
            <span>${this.projectArea} м&#178</span>
</div></a>
        `;
        } else {
            this.elem.innerHTML = `
          <!-- Add Arrows -->
            <div class="mini-slider__swiper-button-next mini-slider__swiper-button-next-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="img/slider-next-arrow.svg" alt=""></div></div>
            <div class="mini-slider__swiper-button-prev mini-slider__swiper-button-prev-${this.numberOfSlider}"> <div class="mini-slider__arrow-wrapper"> <img src="img/slider-prev-arrow.svg" alt=""></div></div>

            <div class="mini-slider__main">
                <div class="mini-slider__swiper-container mini-slider__swiper-container-${this.numberOfSlider}">
                    <!-- Additional required wrapper -->
                    <div class="mini-slider__swiper-wrapper swiper-wrapper">
                        <!-- Slides -->
                         ${this.renderImages().join(' ')}

                    </div>
                    <!-- If we need pagination -->
<!--                    <div class="mini-slider__swiper-pagination"></div>-->
                </div>
            </div>
            <a href="${this.projectLink}"><div class="mini-slider__description">
            <span>${this.projectDescription}</span>
            <span>${this.projectArea} м&#178</span>
</div></a>
        `;

        }

        return this.elem;
    }

    swiperInit() {
        let mySwiper = new Swiper(`.mini-slider__swiper-container-${this.numberOfSlider}`, {
            // Optional parameters
            direction: 'horizontal',
            loop: true,

            navigation: {
                nextEl: `.mini-slider__swiper-button-next-${this.numberOfSlider}`,
                prevEl: `.mini-slider__swiper-button-prev-${this.numberOfSlider}`,
            },
        });
    }

    renderImages() {
        const array = [];

        if (this.portfolio) {

            console.log('PORTFOLIO');
            console.log('imgQuantity', this.imgQuantity);
            if (this.imgQuantity > 0) {
                for (let i = 1; i <= this.imgQuantity; i++) {
                    array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="../mini-slider/img/${this.category}/${this.numberOfSlider}/${i}.${this.imageFormat}"></div>`;
                }
            } else {
                array[0] = `<div class="pre-slide"><img class="slide-image" src="../mini-slider/img/coming-soon.${this.imageFormat}" alt="Coming Soon"></div>`;

            }
        } else {
            if (this.imgQuantity > 0) {
                for (let i = 1; i <= this.imgQuantity; i++) {
                    array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="mini-slider/img/${this.category}/${this.numberOfSlider}/${i}.${this.imageFormat}"></div>`;
                }
            } else {
                array[0] = `<div class="pre-slide"><img class="slide-image" src="mini-slider/img/coming-soon.${this.imageFormat}" alt="Coming Soon"></div>`;
                if(swiperFraction){
                    swiperFraction.display = 'none'; //hide swiper fraction
                }
            }
        }

        return array;
    }

    init() {
        setTimeout(() => {
            console.log(this.containerId);
            document.getElementById(`${this.containerId}`).append(this.render());
            setTimeout(() => {
                this.swiperInit();
            }, 20)
        }, 1000);


    }

}


function createMiniSliders(category, imageFormat = 'jpg') {
    let i = 1;
    miniSliderParam[category].forEach(element => {
        element.containerId = `mini-slider-container-${i}`;
        element.numberOfSlider = `${i}`;
        element.category = category;
        element.imageFormat = imageFormat;
        new MiniSLider(element).init();
        i++;
    });
}

function changeMiniSlider(category, imageFormat = 'jpg') {
    const miniSliderContainers = document.querySelectorAll('.mini-slider-container');
    let counter = 1;
    miniSliderContainers.forEach(container => {

        createPreShowContainer(category, container, counter);
        if (container.querySelector('.mini-slider__main-container')) {
            container.querySelector('.mini-slider__main-container').classList.add('mini-slider__fade-out');
        }
        setTimeout(() => {
            container.innerHTML = '';
        }, 450);
        counter++;

    });
    setTimeout(() => {
        createMiniSliders(category, imageFormat);
    }, 500);
}

function createPreShowContainer(category, container, counter, imageFormat = 'jpg') {
    if (+miniSliderParam[category][counter - 1].imgQuantity > 0) {
        container.setAttribute('style', `background: url("mini-slider/img/${category}/${counter}/1.${imageFormat}");`)
    } else {
        container.setAttribute('style', `background: url("mini-slider/img/coming-soon.${imageFormat}");`)

    }
}


export {MiniSLider};

