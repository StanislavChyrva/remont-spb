class PortfolioSlider {
    constructor({numberOfImages, imgFormat = 'jpg', alt = ''}) {
        this.numberOfImages = numberOfImages;
        this.imgFormat = imgFormat;
        this.alt = alt;
    }

    render() {
        const swiper = document.createElement('div');
        swiper.classList.add('swiper-container');
        swiper.innerHTML = `
 
    <div class="swiper-wrapper">
            <!-- Slides -->
            ${this.renderImages().join(' ')}
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>
    
    `;
        console.log('swiper return');
        return swiper;
    }

    renderImages() {
        const array = [];
        // если изображение в папке больше нуля (Объект параметров.текущая категория.imgCounter[текущий индекс]
        if (this.numberOfImages > 0) {
            for (let i = 1; i <= this.numberOfImages; i++) {
                array[i - 1] = `<div class="swiper-slide"><img class="slide-image" src="img/portfolio-slider/${i}.${this.imgFormat}" alt="${this.alt}"></div>`;
            }
        } else {
            array[0] = `<div class="pre-slide"><img class="slide-image" src="img/portfolio-slider/coming-soon.${this.imgFormat}" alt="Coming Soon"></div>`;
            swiperFraction.display = 'none'; //hide swiper fraction
        }
        return array;
    }

    init() {
        let mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });
    }

}