
containerId = 'portfolio__description';

class Component {
    constructor(tag, className, content, containerId) {
        this.tag = tag;
        this.className = className;
        this.content = content;
        this.containerId = containerId;
        this.elem = null;
    }

    render () {
        this.elem = document.createElement(`${this.tag}`);
        this.elem.className = this.className;
        this.elem.innerHTML = this.content;
        return this.elem;
    }

    insert(){
        this.render();
        document.getElementById(`${this.containerId}`).append(this.elem);
    }

}

//
// new Component('h1', '', 'Ремонт 2-х комнатной квартиры в ЖК «Царская столица» «Комфорт» класс', containerId).insert();
// new Component('h2', '', 'Комфорт» класс', containerId).insert();
// new Component('p', '', '2-х комнатная квартира 15 м²', containerId).insert();
// new Component('p', '', 'ЖК «Царская столица», ул.Кременчугская, д.11', containerId).insert();
// new Component('p', '', 'Сроки работ: 90 дней', containerId).insert();
